/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	一个通用的启动引导和内核初始化的错误定义头文件 
 *
 *  Copyright (c) 2023-2025 tybitsfox <tybitsfox@126.com>
 */
#error number defined
.ifndef	_TOYS_ERRNO_
_TOYS_ERRNO_	=	2024
_err_oom		=		0x1234			#memory request at least 8M
_err_lhd		=		0x2345			#load head error
_err_inm		=		0x9a00a9		#clear memory area error
_err_gdt		=		0x9b00b9		#create gdt error
_err_ldt		=		0x9c00c9		#create ldt error
_err_idt		=		0x9d00d9		#create idt error
_err_tss		=		0x9e00e9		#create tss error
_err_pge		=		0x9f00f9		#create pdt error
_err_gdt_seg	=		0x9b11b9		#create gdt segment descriptor error
_err_gdt_gat	=		0x9b22b9		#create gdt gate descriptor error




.endif

