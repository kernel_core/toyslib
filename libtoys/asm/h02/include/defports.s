/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	各设备端口的定义文件，这个文件不一定直接引用，便于对端口进行查询
 *
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
//{{{ 8253定时器的端口及命令定义	Port:0x40-0x43
.ifdef	_TOYS_8253_
FRQ_8253			=	11930			#定时器频率设定，1/100秒
CMD_8253			=	0x36			#初始化定时器命令字：访问锁存器0,读写LSB/MSB,模式3,二进制。
CMD_PORT_8253		=	0x43			#命令寄存器端口地址
DAT_LCK0_8253		=	0x40			#锁存器0端口地址;	锁存器1：0x41;锁存器2：0x42
.endif
//}}}
//{{{ 8259a中断控制器的端口及命令定义	Ports: 0x20,0x21,0xa0,0xa1
.ifdef	_TOYS_8259A_
MASTER_A00_PORT	=	0x20					#主芯片端口 a0=0，用于发送icw1，ocw2
MASTER_A01_PORT	=	0x21					#主芯片端口 a0=1，用于发送icw2，icw3，icw4，ocw1
SLAVE_A00_PORT	=	0xa0					#从芯片端口 a0=0，用于发送icw1，ocw2
SLAVE_A01_PORT	=	0xa1					#从芯片端口 a0=1，用于发送icw2，icw3，icw4，ocw1
ICW1			=	0x11					#icw1命令：边沿触发中断，多片级联，需要icw4
ICW2_MASTER		=	0x20					#icw2命令：设定主芯片的中断处理起始号：0x20
ICW2_SLAVE		=	0x28					#icw2命令：设定从芯片的中断处理起始号：0x28
ICW3_MASTER		=	4						#icw3命令：设定级联端口，主芯片：4
ICW3_SLAVE		=	2						#icw3命令：设定级联端口，从芯片：2
ICW4			=	1						#icw4命令：设定工作模式：普通全嵌套，非缓冲，非自动结束，8086模式
OCW1_MASK		=	0xff					#ocw1命令：中断屏蔽
OCW1_UNMASK		=	0						#ocw1命令：中断非屏蔽
OCW2			=	0x20					#ocw2命令：非自动结束方式，发送EIO通知中断处理完成
.endif
//}}}
//{{{ 8237 DMA端口及命令定义	Ports: 0~~0xf 0x80~~0x8f
/*Generic Registers		通用寄存器
 DMAC 0 Port (Slave)	DMAC 1 Port (Master)	Descripton
	0x08					0xD0				Status Register (Read)
	0x08					0xD0				Command Register (Write)
	0x09					0xD2				Request Register (Write)
	0x0A					0xD4				Single Mask Register (Write)
	0x0B					0xD6				Mode Register (Write)
	0x0C					0xD8				Clear Byte Pointer Flip-Flop (Write)
	0x0D					0xDA				Intermediate Register (Read)
	0x0D					0xDA				Master Clear (Write)
	0x0E					0xDC				Clear Mask Register (Write)
	0x0F					0xDE				Write Mask Register (Write)
 */
.ifdef	_TOYS_8237A_
DMA0_STATUS_REG					=			0x08		#状态寄存器端口-只读(in)
DMA0_COMMAND_REG				=			0x08		#命令寄存器端口-只写(write)
DMA0_REQUEST_REG				=			0x09		#请求寄存器端口-只写
DMA0_CHANMASK_REG				=			0x0a		#单通道屏蔽寄存器端口-只写
DMA0_MODE_REG					=			0x0b		#模式寄存器端口-只写
DMA0_CLEARBYTE_REG				=			0x0c		#清字节高低位指示寄存器端口-任意只写，
DMA0_TEMP_REG					=			0x0d		#过渡寄存器端口-只读；无用
DMA0_MASTER_CLEAR_REG			=			0x0d		#全通道屏蔽寄存器-任意只写;reset all，开始设置
DMA0_CLEAR_MASK_REG				=			0x0e		#全通道解禁寄存器-任意只写；设置完成，解屏蔽
DMA0_MASK_REG					=			0x0f		#??? unknown
/*	hese values match up with the table above. Now for DMAC 2...
 	DMA1_STATUS_REG = 0xd0,
	DMA1_COMMAND_REG = 0xd0,
	DMA1_REQUEST_REG = 0xd2,
	DMA1_CHANMASK_REG = 0xd4,
	DMA1_MODE_REG = 0xd6,
	DMA1_CLEARBYTE_FLIPFLOP_REG = 0xd8,
	DMA1_INTER_REG = 0xda,
	DMA1_UNMASK_ALL_REG = 0xdc,
	DMA1_MASK_REG = 0xde
 */
/* channel registers	通道寄存器
DMAC 0 Port (Slave)	DMAC 1 Port (Master)	Descripton
	0x0					0xC0				Channel 0 Address/Channel 4 Address
	0x1					0xC2				Channel 0 Counter/Channel 4 Counter
	0x2					0xC4				Channel 1 Address/Channel 5 Address
	0x3					0xC6				Channel 1 Counter/Channel 5 Counter
	0x4					0xC8				Channel 2 Address/Channel 6 Address
	0x5					0xCA				Channel 2 Counter/Channel 6 Counter
	0x6					0xCC				Channel 3 Address/Channel 7 Address
	0x7					0xCE				Channel 3 Counter/Channel 7 Counter   
 */
DMA0_CHAN2_ADDR_REG				 =			4			#floppy used
DMA0_CHAN2_COUNT_REG			 =			5			#floppy	used
/* for DMAC 2...
	DMA1_CHAN4_ADDR_REG = 0xc0,
	DMA1_CHAN4_COUNT_REG = 0xc2,
	DMA1_CHAN5_ADDR_REG = 0xc4,
	DMA1_CHAN5_COUNT_REG = 0xc6,
	DMA1_CHAN6_ADDR_REG = 0xc8,
	DMA1_CHAN6_COUNT_REG = 0xca,
	DMA1_CHAN7_ADDR_REG = 0xcc,
	DMA1_CHAN7_COUNT_REG = 0xce,   
 */
/*Extended Page Address Registers 扩展的页（段，高4位）地址寄存器
  Port	Descripton
0x80	Channel 0 (Original PC) / Extra / Diagnostic port
0x81	Channel 1 (Original PC) / Channel 2 (AT)
0x82	Channel 2 (Original PC) / Channel 3 (AT)
0x83	Channel 3 (Original PC) / Channel 1 (AT)
0x84	Extra
0x85	Extra
0x86	Extra
0x87	Channel 0 (AT)
0x88	Extra
0x89	Channel 6 (AT)
0x8A	Channel 7 (AT)
0x8B	Channel 5 (AT)
0x8C	Extra
0x8D	Extra
0x8E	Extra
0x8F	Channel 4 (AT) / Memory refresh / Slave Connect
 */
DMA_PAGE_CHAN2_ADDRBYTE2		=			0x81	 #floppy used 
/*Command Register 命令寄存器 命令字释义
This register is used to control the DMAC. It has the following format:

    Bit 0: MMT Memory to Memory Transfer
        0: Disable
        1: Enable
    Bit 1: ADHE Channel 0 Address Hold
        0: Disable
        1: Enable
    Bit 2: COND Controller Enable
        0: Disable
        1: Enable
    Bit 3: COMP Timing
        0: Normal
        1: Compressed
    Bit 4: PRIO Priority
        0: Fixed Priority
        1: Normal Priority
    Bit 5: EXTW Write Selection
        0: Late Write Selection
        1: Extended Write Selection
    Bit 6: DROP DMA Request (DREQ)
        0: DREQ sense active high
        1: DREQ sense active low
    Bit 7: DACKP DMA Acknowledge (DACK)
        0: DACK sense active low
        1: DACK sense active high
常用的组合：0x14 or 0x4 允许控制，进入设置模式
			0x10 or 0	关闭控制，进入工作模式
 */
DMA_CMD_MASK_MEMTOMEM			=			1
DMA_CMD_MASK_CHAN0ADDRHOLD		=			2
DMA_CMD_MASK_ENABLE				=			4			#控制开关，常用
DMA_CMD_MASK_TIMING				=			8
DMA_CMD_MASK_PRIORITY			=			0x10		#优先级，常用
DMA_CMD_MASK_WRITESEL			=			0x20
DMA_CMD_MASK_DREQ				=			0x40
DMA_CMD_MASK_DACK				=			0x80
/*Mode Register (Write) 模式寄存器-只写   设置工作模式
    Bits 0-1: SEL0, SEL1 Channel Select
        00: Channel 0
        01: Channel 1
        10: Channel 2
        11: Channel 3
    Bits 2-3: TRA0, TRA1 Transfer Type
        00: Controller self test
        01: Write Transfer from floppy to memory
        10: Read Transfer  from memory to floppy
        11: Invalid
    Bit 4: AUTO Automatic reinitialize after transfer completes (Device must support!)
    Bit 5: IDEC
    Bits 6-7: MOD0, MOD 1 Mode
        00: Transfer on Demand
        01: Single DMA Transfer
        10: Block DMA Transfer
        11: Cascade Mode
normally read from floppy choose: 0x46;write to floppy choose:0x4a		
 */
DMA_MODE_READ_TRANSFER				=			4
DMA_MODE_WRITE_TRANSFER				=			8
DMA_MODE_MASK_AUTO					=			0x10
DMA_MODE_MASK_IDEC					=			0x20
DMA_MODE_TRANSFER_SINGLE			=			0x40
DMA_MODE_TRANSFER_BLOCK				=			0x80
DMA_MODE_CHANNEL2_READ				=			6				#from floppy
DMA_MODE_CHANNEL2_WRITE				=			0xa				#from memory
/*Channel Mask Register (Write)	通道屏蔽寄存器-只写  命令字
 This register allows you to be able to mask a single DMA channel. Bits 0 and 1 allow you to set the channel (00=channel 0, 01=channel 1, 10=channel 2, 11=channel 3). Bit 4 determins weather to mask or unmask the channel. If bit 4 is 0, it unmasks the channel. If it is 1, it will mask it. All other bits are unused.

    Bit 0-1: Channel select
    Bit 2: 0=unmasks channel, 1=masks channel

All other bits unused.
 */

/*Mask Register (Write)	全通道屏蔽寄存器-只写  命令字
This register containes information on what channels are currently masked and unmasked. The top 4 bits in this 8 bit register are always unused. The low four bits are used to mask or unmask one of the four channels. For example, bit 0 is for channel 0, bit 1 is for channel 1, and so on. Note: Masking channel 4 will also mask channels 4,5,6,7 due to cascading.

    Bit 0: Channel select 0
    Bit 1: Channel select 1
    Bit 2: Channel select 2
    Bit 3: Channel select 3

All other bits unused. There are two ports for both DMACs: 0xa and 0xd4
 */

/*Clear Byte Pointer Flip-Flop	清字节高低位指针，只要往端口写入即可，随意写
This is a special i/o address port that allows us to control the flip-flop between 16 bit transfers when working with the 8 bit DMAC (the primary DMAC.)

There are two ports for both DMACs:  0x0c and 0xD8,
 */
/*Reset		重置寄存器-只写  命令字 随意写
In a very similar fashion, you can reset the a DMAC by writing any value to the following registers: 
Ports is : 0x0D
 */
/*Unmask All Registers	解禁所有寄存器 命令字 随意写
In yet another similar fashion, the same concept applies with this command! Wouldn't it be great if all hardware programming commands were this easy? ;) 
Ports is : 0x0E, 
this will unmask all registers from the slave DMAC
 */

.endif
//}}}
//{{{ 82072A FDC 软驱控制器端口及命令定义 Ports: 0x370~~0x377 0x3F0~~0x3f7
.ifdef	_TOYS_FDC_
/*FDC Port mapping		端口映射
					Floppy Disk Controller Ports
Port (FDC 0)	Port (FDC 1)		Read/Write				Descripton
					Primary FDC Registers
0x3F2			0x372			Write Only					Digital Output Register (DOR)
0x3F4			0x374			Read Only					Main Status Register (MSR)
0x3F5			0x375			Read / Write				Data Register
0x3F7			0x377			Read Only					AT only. Configuation Control Register (CCR)
0x3F7			0x377			Write Only					AT only. Digital Input Register (DIR)
					Other FDC Registers
0x3F0			0x370			Read Only					PS/2 only. Status Register A (SRA)
0x3F1			0x371			Read Only					PS/2 only. Status Register B (SRB)
0x3F4			0x374			Write Only					PS/2 only. Data Rate Select Register (DSR)
 */
FLPYDSK_DOR					=			0x3f2
FLPYDSK_MSR					=			0x3f4
FLPYDSK_FIFO				=			0x3f5	#data register
FLPYDSK_CTRL				=			0x3f7
/*Digital Output Register (DOR)  DOR状态命令字定义,用于指定驱动器，工作模式，指定驱动器的马达启动和关闭
 This is a write only register that allows you to control different functions of the FDC, such as the FDD motor control, operation mode (DMA and IRQ), reset, and drive. It has the format:

    Bits 0-1 DR1, DR2
        00 - Drive 0
        01 - Drive 1
        10 - Drive 2
        11 - Drive 3
    Bit 2 REST
        0 - Reset controller
        1 - Controller enabled
    Bit 3 Mode
        0 - IRQ channel
        1 - DMA mode
    Bits 4 - 7 Motor Control (Drives 0 - 3)
        0 - Stop Motor for drive
        1 - Start Motor for drive
*/
FLPYDSK_DOR_MASK_DRIVE0			=	0	#00000000	= here for completeness sake
FLPYDSK_DOR_MASK_DRIVE1			=	1	#00000001
FLPYDSK_DOR_MASK_DRIVE2			=	2	#00000010
FLPYDSK_DOR_MASK_DRIVE3			=	3	#00000011
FLPYDSK_DOR_MASK_RESET			=	4	#00000100		motor on/off used
FLPYDSK_DOR_MASK_DMA			=	8	#00001000		motor on	 used
FLPYDSK_DOR_MASK_DRIVE0_MOTOR		=	16	#00010000	motor on	 used		driver 0: floppy A
FLPYDSK_DOR_MASK_DRIVE1_MOTOR		=	32	#00100000							driver 1: floppy B
FLPYDSK_DOR_MASK_DRIVE2_MOTOR		=	64	#01000000
FLPYDSK_DOR_MASK_DRIVE3_MOTOR		=	128	#10000000
/*Main Status Register (MSR)	主状态寄存器状态命令字
The Main Status Register (MSR) follows a *gasp!* specific bit format! Bet you did not see that one coming! Okay, okay, lets get back on track here (pun intended). Here is the format of the MSR:

    Bit 0 - FDD 0: 1 if FDD is busy in seek mode
    Bit 1 - FDD 1: 1 if FDD is busy in seek mode
    Bit 2 - FDD 2: 1 if FDD is busy in seek mode
    Bit 3 - FDD 3: 1 if FDD is busy in seek mode
        0: The selected FDD is not busy
        1: The selected FDD is busy
    Bit 4 - FDC Busy; Read or Write command in progress
        0: Not busy
        1: Busy
    Bit 5 - FDC in Non DMA mode
        0: FDC in DMA mode
        1: FDC not in DMA mode
    Bit 6 - DIO: direction of data transfer between the FDC IC and the CPU
        0: FDC expecting data from CPU
        1: FDC has data for CPU
    Bit 7 - RQM: Data register is ready for data transfer
        0: Data register not ready
        1: Data register ready

This MSR is a simple one. It containes the current status information for the FDC and disk drives. Before sending a command or reading from the FDD, we will need to always check the current status of the FDC to insure it is ready.

Here is an example of reading from this MSR to see if its busy. We first define the bit masks that will be used in the code. Notice how it follows the format shown above.   
use as:
if ( inportb (FLPYDSK_MSR) & FLPYDSK_MSR_MASK_BUSY )
	//! FDC is busy
*/
	FLPYDSK_MSR_MASK_DRIVE1_POS_MODE	=	1	#00000001
	FLPYDSK_MSR_MASK_DRIVE2_POS_MODE	=	2	#00000010
	FLPYDSK_MSR_MASK_DRIVE3_POS_MODE	=	4	#00000100
	FLPYDSK_MSR_MASK_DRIVE4_POS_MODE	=	8	#00001000
	FLPYDSK_MSR_MASK_BUSY			=	16	#00010000
	FLPYDSK_MSR_MASK_DMA			=	32	#00100000
	FLPYDSK_MSR_MASK_DATAIO			=	64 	#01000000
	FLPYDSK_MSR_MASK_DATAREG		=	128	#10000000
/*Data Register		数据寄存器 	命令和状态字的收发
This is a 8 or 16 bit read/write register. The actual size of the register is specific on the type of controller. All command paramaters and disk data transfers are read to and written from the data register. This register does not follow a specific bit format and is used for generic data. It is accessed through I/O port 0x3f5 (FDC 0) or 0x375 (FDC 1).

Note: Before reading or writing this register, you should always insure it is valid by first reading its status in the Master Status Register (MSR).

Remember: All command bytes and command paramaters are sent to the FDC through this register! You will see examples of this in the command section below, so dont worry to much about it yet.

If an invalid command was issued, the value returned from the data register is 0x80.  
*/
/*Configuation Control Register (CCR)	配置控制寄存器状态命令字
In PC/AT Mode, this register is known as the Data Rate Select Register (DSR) and only has the first two bits set (Bit 0=DRATE SEL0, Bit 1=DRATE SEL1.) This was listed in a table in the DSR register section. Lets take another look...
    00 500 Kbps
    10 250 Kbps
    01 300 Kbps
    11 1 Mbps
Bit 2 is NOPREC in Model 30/CCR modes and has no function. Other bits are undefined and may change depending on controller. 
*/	
/*Commands		操作命令字定义
Commands are used to control a FDD connected to the FDC for different operations, like reading and writing. They are written to the data register over the data bus (D0-D7) pins during a write operation (IO and WRITE control lines are set on the control bus.) In other words, a OUT assembly language instruction to the data register at port 0x3f5 (FDC 0) or 0x375 (FDC 1.)

Warning: Before sending a command or paramamter byte, insure the data register is ready to recieve data by testing bit 7 of the Main Status Register (MSR) first.
*/
FDC_CMD_READ_TRACK			=		2			#读磁道
FDC_CMD_SPECIFY				=		3			#传递控制信息（磁头加、卸载速率，单步速率，DMA模式）给软驱
FDC_CMD_CHECK_STAT			=		4			#检测状态
FDC_CMD_WRITE_SECT			=		5			#写扇区
FDC_CMD_READ_SECT			=		6			#读扇区
FDC_CMD_CALIBRATE			=		7			#
FDC_CMD_CHECK_INT			=		8			#中断检测
FDC_CMD_WRITE_DEL_S			=		9			#写删除
FDC_CMD_READ_ID_S			=		0xa			#读id
FDC_CMD_READ_DEL_S			=		0xc			#读删除
FDC_CMD_FORMAT_TRACK		=		0xd			#格式化
FDC_CMD_SEEK				=		0xf			#磁头定位
/*Extended Command Bits		扩展的命令字
Some of these commands require you to pass several bytes before the command is executed. Others return several bytes. To make things easier to read, I have listed all of the commands, formats, and paramater bytes in tables. Each command comes with an explination and an example routine.

Okay, now remember when we mentioned exteneded command bits and how the commands above are only four bits? The upper four bits can be used for different things and purposes.

When describing the format of a command, we represent an extended bit with a character (like M or F.) For example, the Write Sector command has the format M F 0 0 0 1 1 0, where the first four bits (0 1 1 0) are the command byte and the top four bits, M F 0 0 represent different settings. M is set for multitrack, F to select what density mode to operate in for the command. 
 Here is a list of common bits:

    M - MultiTrack Operation
        0: Operate on one track of the cylinder
        1: Operate on both tracks of the cylinder
    F - FM/MFM Mode Setting
        0: Operate in FM (Single Density) mode
        1: Operate in MFM (Double Density) mode
    S - Skip Mode Setting
        0: Do not skip deleted data address marks
        1: Skip deleted data address marks
    HD - Head Number
    DR0 - DR1 - Drive Number Bits (2 bits for up to 4 drives)

The M, F, and S bits are very common to alot of the commands, so I decided to stick them in a nice enumeration. To set them, just bitwise OR these settings with the command that you would like to use. */
FDC_CMD_EXT_SKIP			=	0x20	#00100000
FDC_CMD_EXT_DENSITY			=	0x40	#01000000
FDC_CMD_EXT_MULTITRACK		=	0x80	#10000000
/*GAP 3		物理扇区之间的间隔
GAP 3 referrs to the space between sectors on the physical disk. It is a type of GPL (Gap Length). 
*/
FLPYDSK_GAP3_LENGTH_STD		=	 42
FLPYDSK_GAP3_LENGTH_5_14	=	 32
FLPYDSK_GAP3_LENGTH_3_5		=	 27
/*Bytes Per Sector		每扇区字节数
Some commands require us to pass in the bytes per sector. These cannot be any size, however, and always follows a formula:
    2^n * 128, where ^ denotes "to the power of"
n is a number from 0-7. It cannot go higher then 7, as 2^7 * 128 = 16384 (16 kbytes). It is possible to select up to 16 Kbytes per sector on the FDC. Most drives may not support it, however.  
...So, if a command requires us to pass the number of bytes per sector, dont put 512! rather, put FLPYDSK_SECTOR_DTL_512, which is 2. 
*/
FLPYDSK_SECTOR_DTL_128	=	0
FLPYDSK_SECTOR_DTL_256	=	1
FLPYDSK_SECTOR_DTL_512	=	2
FLPYDSK_SECTOR_DTL_1024	=	4
/*Write Sector			写扇区命令字
    Format: M F 0 0 0 1 0 1
    Paramaters:
        x x x x x HD DR DR0
        Cylinder
        Head
        Sector Number
        Sector Size
        Track Length
        Length of GAP3
        Data Length
    Return:
        Return byte 0: ST0
        Return byte 1: ST1
        Return byte 2: ST2
        Return byte 3: Current cylinder
        Return byte 4: Current head
        Return byte 5: Sector number
        Return byte 6: Sector size

This command writes a sector from a FDD. For every byte in the sector, the FDC issues interrupt 6 and places the byte read from the disk into the data register so that we can read it in. 
*/
/*Read Sector

    Format: M F S 0 0 1 1 0
    Paramaters:
        x x x x x HD DR1 DR0 = HD=head DR0/DR1=Disk
        Cylinder
        Head
        Sector Number
        Sector Size
        Track Length
        Length of GAP3
        Data Length
    Return:
        Return byte 0: ST0
        Return byte 1: ST1
        Return byte 2: ST2
        Return byte 3: Current cylinder
        Return byte 4: Current head
        Return byte 5: Sector number
        Return byte 6: Sector size

This command reads a sector from a FDD. For every byte in the sector, the FDC issues interrupt 6 and places the byte read from the disk into the data register so that we can read it in.
*/
/*Fix Drive Data / Specify		Note:传递控制信息命令字

    Format: 0 0 0 0 0 0 1 1
    Paramaters:
        S S S S H H H H - S=Step Rate H=Head Unload Time
        H H H H H H H NDM - H=Head Load Time NDM=0 (DMA Mode) or 1 (DMA Mode)
    Return: None
  
 */
 /*Check Status		Note:检测状态命令

    Format: 0 0 0 0 0 1 0 0
    Paramaters:
        x x x x x HD DR1 DR0
    Return:
        Byte 0: Status Register 3 (ST3)
This command returns the drive status. 
*/
/*Calibrate Drive		Note:校准驱动器命令字

    Format: 0 0 0 0 0 1 1 1
    Paramaters:
        x x x x x 0 DR1 DR0 
    Return: None
*/
/*Check Interrupt Status	Note: 检测中断状态

    Format: 0 0 0 0 1 0 0 0
    Paramaters: None
    Return:
        Byte 0: Status Register 0 (ST0)
        Byte 1: Current Cylinder

*/
/*Seek / Park Head		Note:磁头定位

    Format: 0 0 0 0 1 1 1 1
    Paramaters:
        x x x x x HD DR1 DR0 - HD=Head DR1/DR0 = drive
        Cylinder
    Return: None
*/
/*Resetting the FDC		Note:重置FDC控制器
Disabling the Controller: flpydsk_write_dor (0);
Enabling the Controller: flpydsk_write_dor ( FLPYDSK_DOR_MASK_RESET | FLPYDSK_DOR_MASK_DMA); 
*/
.endif
//}}}
//{{{显示控制器端口	0x3d4,0x3d5
CURSOR_PORT1	=	0x3d4		#显示模式3下的光标设置端口
CURSOR_PORT2	=	0x3d5

//}}}
//{{{AT硬盘控制器端口及命令定义		Port:0x1f0-0x1f7;0x3f6;0x3f7
.ifdef	_TOYS_HDD_
HDP_DATA		=	0x1f0			#数据寄存器 -扇区数据（读、写、格式化）insw,outsw
HDP_PRECOMP		=	0x1f1			#写补偿寄存器	（只写操作；only outb）
HDP_ERR			=	0x1f1			#错误寄存器,错误代码(只读操作 only inb)
HDP_SECTC		=	0x1f2			#扇区数寄存器 -扇区数（读、写、校验、格式化）
HDP_SECTB		=	0x1f3			#扇区号寄存器 -起始扇区（读、写、校验、格式化）
HDP_CYLL		=	0x1f4			#柱面号寄存器 -柱面号低字节（读、写、校验、格式化）
HDP_CYLH		=	0x1f5			#柱面号寄存器 -柱面号高字节（读、写、校验、格式化）
HDP_DRVHD		=	0x1f6			#驱动器/磁头寄存器 -驱动器号/磁头号（101dhhhh,d=驱动器，h=磁头号）
HDP_STAT		=	0x1f7			#主状态寄存器（只读操作）
HDP_EXE			=	0x1f7			#命令寄存器 (只写操作)
HDP_CMD			=	0x3f6			#硬盘控制寄存器	(只写操作；only outb)
/*硬盘控制器错误代码：
	
值		诊断命令时		其他命令时 		值		诊断命令时		其他命令时
0x01	无错误			数据标志丢失 	0x05	控制处理器错	
0x02	控制器出错		磁道0错 		0x10					ID未找到
0x03	扇区缓冲区错					0x40					ECC错误
0x04	ECC部件错误		命令放弃 		0x80					坏扇区	
*/
HDC_EOK			=	1		#无错误
HDC_ECTL		=	2		#控制器出错
HDC_EBUF		=	3		#扇区缓冲区错
HDC_ECC			=	4		#ECC部件错误
HDC_ERR			=	5		#控制处理器错
HDC_EID			=	0x10	#ID未找到
HDC_EECC		=	0x40	#ECC错误
HDC_EBAD		=	0x80	#坏扇区
/*主状态寄存器,状态字节含义：
位	linux名称	屏蔽码	说明
0	ERR_STAT	0x1		命令执行错误
1	INDEX_STAT	0x2		收到索引
2	ECC_STAT	0x4		ECC校验错误
3	DRQ_STAT	0x8		数据请求服务，该位置位时表示数据端口有数据可传输
4	SEEK_STAT	0x10	驱动器寻道结束
5	WRERR_STAT	0x20	驱动器故障（写出错）
6	READY_STAT	0x40	驱动器准备好（就绪）
7	BUSY_STAT	0x80	控制器忙碌  
*/
HDST_ERR			=	1
HDST_IDX			=	2
HDST_ECC			=	4
HDST_DRQ			=	8
HDST_SEEK			=	0x10
HDST_DERR			=	0x20
HDST_READY			=	0x40
HDST_BUSY			=	0x80
/*硬盘控制命令，共有8种命令：
 
*/
HDEXE_REST			=	0x10			#驱动器复位
HDEXE_READ			=	0x20
HDEXE_WRITE			=	0x30			
HDEXE_VERIFY		=	0x40			#扇区检查
HDEXE_FORMAT		=	0x50			#磁道格式化
HDEXE_INIT			=	0x60			#控制器初始化
HDEXE_SEEK			=	0x70			#寻道
HDEXE_DIAG			=	0x90			#控制器诊断
HDEXE_SPEC			=	0x91			#建立驱动器参数
/*0x3f6硬盘控制寄存器：该寄存器是只写的，用于存放硬盘控制字节并控制复位操作。
 该控制字节与硬盘参数表中偏移0x8处的字节相同，含义如下：
位0		未用
位1		保留0（关闭IRQ）
位2		允许复位
位3		若磁头数大于8则置1
位4		未用（0）
位5		若在柱面数+1处有生产商的坏区图，则置1
位6		禁止ECC重试
位7		禁止访问重试
 */
HD_CONTROL_BYTE		=	0xc8
.endif
//}}}

