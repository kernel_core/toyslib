/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	本文件实现toys首次进入保护模式的代码，由于在实模式下无法加载1M之外的数据，因此需要在此
 *	继续完成部分初始化工作。本文件主要完成：
 *		1、将设备参数拷贝至最终位置；
 *		2、重新建立GDT；
 *		3、建立一个LDT；
 *		4、由于本文件仍以初始化为主，没有建立设备驱动，无法加载硬盘的数据；但在实模式下我们采
 *	用bios的设备驱动多加载了14个扇区的正式保护模式的代码，这里还要将这些代码移动至目标位置。
 *	完成进入真正head的初始化。
 *		5、在进入真正的head以后，本文件下生成的代码和数据以及实模式下的代码和数据将被丢弃，所
 *	占空间将收回另派他用。
 *	2024-1-13	update
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifndef	_TOYS_MEM_STAB
_TOYS_MEM_STAB	=	2024			#因为需要将数据和代码加载至stable head的位置
.endif
_TOYS_TEMP_HEAD	=	2024			#redirection need 
.include "defconst.s"
.include "deferrno.s"
.include "defmem.s"
.text
	cld
	movl $GDT_DS,%eax
	movw %ax,%ds
	movw %ax,%es
	movw %ax,%fs
	movw %ax,%gs
	movl $KM_BUF,%edi
	movl $0xffff,%eax
	movl %eax,(%edi)
	movl $GDT_SS,%eax
	movl %eax,4(%edi)
	lss (%edi),%esp
	movl $KM_BASE,%edi
	movl $KM_LEN,%ecx
	shrl $2,%ecx
	call _sys_clear
	movl $_err_inm,%eax
	jc 9f
	call copy_bios_para
	movl $IDT_BEG,%edi
	movl $BITMAP_BEG,%ecx
	subl %edi,%ecx
	shrl $2,%ecx
	call _sys_clear
	movl $_err_inm,%eax
	jc 9f
	call crt_gdt





9:
	jmp .

//{{{function:	_sys_clear		内核缓冲区的初始化函数
/*寄存器传递参数
  in:		edi:	bufferaddress
  			ecx:	count of long;max allowed length=64k
  out:		CF	clear if success			
 */	
_sys_clear:
	pushl %eax
	cmpl $0x40000,%ecx			#allowed size: 1M bytes
	ja 8f
	cmpl $TEXT_RUN,%edi			#allowed area:	0~3M
	jae 8f
	xorl %eax,%eax
	rep stosl
	clc
	jmp 9f
8:
	stc
9:
	popl %eax
	ret
//}}}
//{{{function:	copy_bios_para		#copy floppy&hd parameters to target
cpoy_bios_para:
	pusha
	movl $0x7e00,%esi
	movl $KM_FLPP,%edi
	movl $12,%ecx
	rep movsb				#floppy
	movl $KM_HDP,%edi
	movl $32,%ecx
	rep movsb				#hd0,1
	movl $0x7e40,%esi
	movl $KM_MEM,%edi
	movl $4,%ecx
	rep movsb				#mem size
	popa
	ret
//}}}
//{{{function: crt_gdt 	#create gdt by crt_segdesc
crt_gdt:
	pushl %ebp
	movl %esp,%ebp
	movl 

	
//}}}




.include "src/kern.s"			#need function:	crt_segdesc

.org	1534
.word	0xbb66

