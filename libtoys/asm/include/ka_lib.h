#kernel libs written by asm
#
#		tybitsfox 20230312	<for toys>
#
.ifdef	KA_SNPRINT
/*
   ka_ means Kernel written by Asm;
   i_ means Index of;
 */
#base on esp
s_ax	=	28
s_cx	=	24
s_dx	=	20
s_bx	=	16
s_sp	=	12
s_bp	=	8
s_si	=	4
s_di	=	0
#base on ebp
b_ax	=	0
b_cx	=	4
b_dx	=	8
b_bx	=	12
b_sp	=	16
b_bp	=	20
b_si	=	24
b_di	=	28
.endif


