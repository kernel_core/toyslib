/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	toys的引导扇区代码，用于加载head并进入保护模式
 *
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */

_TOYS_16_	=	0
.include "defconst.s"
.text
	jmp $BOOTSEG,$go
go:
	mov %cs,%ax
	mov %ax,%ds
	mov %ax,%es
	cld
	mov $0x200,%di
	movw $STKSP,(%di)			#default: ds:di
	movw $STKSEG,2(%di)
	lss (%di),%sp				#ss:sp	=	0x2000:0x3ff
	movw $39,(%di)
	movl $gdt,%eax
	addl $BOOTADDR,%eax
	movl %eax,2(%di)
	lgdt (%di)
	call check_set_vmod
	call get_drv_para
	call get_phy_mem
	call load_head
	call set_8253
	call reset_8259a
	smsw %ax
	or $1,%ax
	lmsw %ax
	jmp $8,$0x9000

//{{{ function check_set_vmod
check_set_vmod:
	push %ds
	mov $0,%ax
	mov %ax,%ds
	mov $0x49,%si
	lodsb
	cmpb $3,%al
	je 1f
	mov $3,%ax
	int $0x10
1:	
	pop %ds
	ret
//}}}	
//{{{ function get_drv_para
get_drv_para:
	push %ds
	mov $0,%ax
	mov %ax,%ds
	mov $0x78,%bx			#int 0x1E
	mov (%bx),%si
	mov 2(%bx),%ax
	mov %ax,%ds
	mov $12,%cx
	mov $BUFFADDR,%di
	rep movsb				#floppy's param
	mov $0,%ax
	mov %ax,%ds
	mov $0x104,%bx			#int 0x41
	mov (%bx),%si
	mov 2(%bx),%ax
	mov %ax,%ds
	mov $BUFFADDR,%di
	add $0x10,%di
	mov $16,%cx
	rep movsb
	mov $0,%ax
	mov %ax,%ds
	mov $0x118,%bx
	mov (%bx),%si
	mov 2(%bx),%ax
	mov $16,%cx
	rep movsb
	pop %ds
	ret
//}}}	
//{{{ function get_phy_mem
get_phy_mem:
	movl $SMAP,%edx
	movl $20,%ecx
	movl $0,%ebx
	movl $0,%esi
1:
	movl $0x400,%edi
	movl $0xe820,%eax
	int $0x15
	jc 2f
	addl 8(%edi),%esi
	cmpl $0,%ebx
	jne 1b
	jmp 3f
2:
	xorl %esi,%esi
3:
	movl $BUFFADDR,%edi
	addl $0x40,%edi
	testl $0x8000,%esi
	jz 4f
	addl $0x8000,%esi
4:
	movl %esi,%eax
	stosl
	ret
//}}}
//{{{ function load_head
load_head:
	mov $0,%dx
	mov $2,%cx
	mov $0x1400,%bx
	mov $0x211,%ax
	int $0x13
	jnc 1f
	mov $0x1234,%ax
	jmp .
1:
	ret
//}}}
//{{{ function set_8253
set_8253:
	movb $CMD_8253,%al
	outb %al,$CMD_PORT_8253
	jmp .+2
	movw $FRQ_8253,%ax
	outb %al,$DAT_LCK0_8253
	jmp .+2
	movb %ah,%al
	outb %al,$DAT_LCK0_8253
	ret
//}}}
//{{{ function reset_8259a
reset_8259a:
	cli
	movb $ICW1,%al
	outb %al,$MASTER_A00_PORT			#send icw1
	jmp .+2
	outb %al,$SLAVE_A00_PORT			#send icw1 slave
	jmp .+2
	movb $ICW2_MASTER,%al
	outb %al,$MASTER_A01_PORT			#send icw2
	jmp .+2
	movb $ICW2_SLAVE,%al
	outb %al,$SLAVE_A01_PORT			#send icw2 slave
	jmp .+2
	movb $ICW3_MASTER,%al
	outb %al,$MASTER_A01_PORT			#send icw3
	jmp .+2
	movb $ICW3_SLAVE,%al
	outb %al,$SLAVE_A01_PORT			#send icw3 slave
	jmp .+2
	movb $ICW4,%al
	outb %al,$MASTER_A01_PORT			#send icw4
	jmp .+2
	outb %al,$SLAVE_A01_PORT			#send icw4 slave
	jmp .+2
	movb $OCW1_MASK,%al
	outb %al,$MASTER_A01_PORT			#send ocw1_mask
	jmp .+2
	outb %al,$SLAVE_A01_PORT			#send ocw1_mask slave
	jmp .+2
	ret
//}}}

gdt:	.word	0,0,0,0
		.word	0x7ff,0,0x9a00,0x00c0		#0x8	text	8M		0~~0x7FFFFF
		.word	0x7ff,0,0x9200,0x00c0		#0x10	data	8M		0~~0x7FFFFF
		.word	0xf,0,0x9208,0x00c0			#0x18	stack	64K 	0x8FFFF~~0x80000
		.word	1,0x8000,0x920b,0x00c0		#0x20	disp	8K		0xb8000~~0xba000

.org 510
.word 0xaa55

