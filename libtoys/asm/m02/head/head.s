/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	toys进入保护模式的head代码，用于在保护模式下继续初始化系统数据结构及安装中断和驱动函数
 *
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifndef	_TOYS_MEM_STAB
_TOYS_MEM_STAB	=	0
.endif
.include "defmem.s"
.include "defconst.s"
	cld
	movl $GDT_DS,%eax
	movw %ax,%ds
	movw %ax,%es
	movw %ax,%fs
	movw %ax,%gs
	movl $KM_BUF,%edi
	movl $0xffff,%eax
	movl %eax,(%edi)
	movl $GDT_SS,4(%edi)
	lss (%edi),%esp
	call copy_param
	call clsscr
	xorl %eax,%eax
	call set_cur_pos
	call init_env
#	call show_01
	call crt_idt
	movl $KM_BUF,%ebx
	movl $0x7ff,%eax
	movw %ax,(%ebx)
	xorl %eax,%eax
	movl %eax,2(%ebx)
	lidt (%ebx)
	outb %al,$0x21
	jmp .+2
	outb %al,$0xa1
	jmp .+2
	sti
	movl $14,%eax
	int $0x80
	movl $0x1234567,%ecx
#	call show_02
	movl umem_begin,%eax
	movl kmem_size,%ebx
	call crt_gdt
	movl $GDT_BEG,%esi
	addl $16,%esi
	movl $16,%ecx
	movl $1,%eax
	call show_msg
	movl $DMA_BASE,%edi
	leal m01,%esi
	movl $len,%ecx
	rep movsb
	movl $4,%eax
#	movl $0x101,%ebx
	movl $0x201,%ebx
	movl $2,%ecx
#	call read_sector
	call write_sector
	jnc	1f
	movl $KM_FLPERR,%esi
	movl (%esi),%eax
	movl 4(%esi),%ebx
	jmp 9f
1:	
	movl $DMA_BASE,%esi
	movl $12,%ecx
	movl $1,%eax
	call show_msg
	movl $0x1234567,%eax
#硬盘启动测试：1、控制器复位；2、硬盘驱动参数建立；
#硬盘写入的测试
	call init_hd
	jnc 3f
	movl $0x222111,%ebx
	movl $KM_HDRT,%edx
	movb (%edx),%al
	jmp 9f
3:
#	call do_write_to_hd
	movl $8,%eax
	int $0x80
	call do_seek				#test ok
	jnc 4f
	movl $0x333333,%ebx
	movl $KM_HDERR,%edx
	movl (%edx),%eax
	jmp 9f
4:	
	movl $0x10,%eax
	int $0x80
	call do_write_to_hd			#test no		错误代码无，但中断未调用
#	call do_read_from_hd		test ok
#	call do_restore_hd			test ok
#	call do_init_hd				test no		错误代码04：命令放弃
#	call do_verf_hd				test ok
#	call do_diag_hd				test ok
#	call do_format_hd			test no		错误代码04
	jnc 5f
	movl $0x343434,%ebx
	movl $KM_HDERR,%edx
	movl (%edx),%eax
	jmp 9f
5:	
	movl $0x111111,%ebx
	movl $KM_HDRT,%edx
	movb (%edx),%al
9:
	jmp .
//{{{ interrupt function	nor_int		所有中断的通用响应函数
nor_int:
	pushal
	push %es
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $460,%edi
	movl $0xc41,%eax
	stosw
	pop %es
	popal
	iret
//}}}	
//{{{ interrupt function nor_8259m_int	8259a主芯片的通用中断响应函数
nor_8259m_int:
	pushal
	push %es
	movl $0x20,%eax
	outb %al,$0x20
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $462,%edi
	movl $0xb42,%eax
	stosw
	pop %es
	popal
	iret
//}}}
//{{{ interrupt function nor_8259s_int	8259a从芯片的通用中断响应函数
nor_8259s_int:
	pushal
	push %es
	movl $0x20,%eax
	outb %al,$0xa0
	jmp .+2
	outb %al,$0x20
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $464,%edi
	movl $0xb43,%eax
	stosw
	pop %es
	popal
	iret
//}}}
//{{{ interrupt function time_int	时钟中断 0x20
time_int:
	pushal
	push %ds
	movl $0x20,%eax
	outb %al,$0x20
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $KM_COUNTER,%ebx
	movl (%ebx),%eax
	incl %eax
	movl %eax,(%ebx)
	pop %ds
	popal
	iret
//}}}	
//{{{ interrupt function sys_int	int 0x80 中断调用
/*in:	al:延时次数，每次1/100秒，最多延时255
		ah:中断号，=0 为延时调用；其余暂无实现
 */	
sys_int:
	pushal
	push %ds
	movl $GDT_DS,%ebx
	movw %bx,%ds
	cmpb $0,%ah
	jne 2f
	andl $0xff,%eax
	movl $KM_COUNTER,%ebx
	addl (%ebx),%eax
1:	
	jmp .+2			#hlt;nop
	cmpl (%ebx),%eax
	ja 1b
2:	
	pop %ds
	popal
	iret
//}}}
//{{{ interrupt function flp_int	软驱中断处理函数
flp_int:
	pusha
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $KM_FINT,%ebx
	movl (%ebx),%eax
	orl $0x80,%eax
	movl %eax,(%ebx)
	movl $0x20,%eax
	outb %al,$0x20
	jmp .+2
	pop %ds
	popa
	iret
//}}}
//{{{ interrupt function hdc_int	硬盘的中断处理函数
hdc_int:
	pushal
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $0x20,%eax
	outb %al,$0xa0
	call hd_normal_int
	movl $0xa45,%ecx
	jc 9f
	movl $KM_HDCMDIDX,%ebx
	cmpl $0,(%ebx)
	jne 1f
	movl $0xa46,%ecx
	jmp 9f
1:	
	cmpl $HDEXE_REST,(%ebx)				#REST
	jne 2f
	movl $0,(%ebx)						#clear command bytes
	movl $0xb61,%ecx					#echo 'a' for test
	movl $KM_HDCMDIDX,%ebx
	movl $0,(%ebx)
	jmp 9f
2:
	cmpl $HDEXE_READ,(%ebx)				#READ
	jne 3f
	call hd_read_int
	movl $0xb62,%ecx
	jmp 9f
3:
	cmpl $HDEXE_WRITE,(%ebx)			#WRITE
	jne 4f
	call hd_write_int
	movl $0xb63,%ecx
	jmp 9f
4:
	cmpl $HDEXE_VERIFY,(%ebx)			#VERIFY
	jne 5f
	movl $0xb64,%ecx
	movl $KM_HDCMDIDX,%ebx
	movl $0,(%ebx)
	jmp 9f
5:
	cmpl $HDEXE_FORMAT,(%ebx)			#FORMAT ----how to?		e
	jne 6f
	movl $0xb65,%ecx
	movl $KM_HDCMDIDX,%ebx
	movl $0,(%ebx)
	jmp 9f
6:
	cmpl $HDEXE_INIT,(%ebx)				#INIT					f
	jne 7f
	movl $0xb66,%ecx
	movl $KM_HDCMDIDX,%ebx
	movl $0,(%ebx)
	jmp 9f
7:
	cmpl $HDEXE_SEEK,(%ebx)				#SEEK					g
	jne 1f
	movl $0xb67,%ecx
	movl $KM_HDCMDIDX,%ebx
	movl $0,(%ebx)
	jmp 9f
1:
	cmpl $HDEXE_DIAG,(%ebx)				#DIAG					h
	jne 2f
	movl $0xb68,%ecx
	movl $KM_HDCMDIDX,%ebx
	movl $0,(%ebx)
	jmp 9f
2:
	cmpl $HDEXE_SPEC,(%ebx)				#SPEC					i
	jne 3f
	movl $KM_HDCMDIDX,%ebx
	movl $0,(%ebx)
	movl $0xb69,%ecx
	jmp 9f
3:
	movl $0xb6a,%ecx					#						j
9:
	push %es
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $468,%edi
	movl %ecx,%eax
	stosw
	pop %es
	movl $0x20,%eax
	outb %al,$0x20
	pop %ds
	popal
	iret
//}}}

//{{{ function copy_param
copy_param:
	movl $KM_BASE,%edi
	movl $KM_LEN,%ecx
	movl $0,%eax
	rep stosb
	movl $0x7e00,%esi
	movl $KM_FLPP,%edi
	movl $0x50,%ecx
	rep movsb
	ret
//}}}
//{{{ function clsscr
clsscr:
	push %es
	movl $GDT_DISP,%eax
	mov %ax,%es
	movl $0,%edi
	movl $0xfa0,%ecx		#起始的1M内存中高于640k的内存尽可能不要改动，bios占用
	movl $0x720,%eax
	rep stosw
	movl $0,%eax
	movl $KM_CURSOR,%edi
	movl %eax,(%edi)
	call set_cur_pos
	pop %es
	ret
//}}}
//{{{ function set_cur_pos
#ax:string length
set_cur_pos:
	pushl %eax
	movl $0xf,%eax
	movl $0x3d4,%edx
	outb %al,%edx
	jmp .+2
	incl %edx
	popl %ebx
	andl $0xff,%ebx		#default max string length:255
	movl $KM_CURSOR,%edi
	addl (%edi),%ebx
	movb %bl,%al
	outb %al,%edx
	jmp .+2
	movb $0xe,%al
	decl %edx
	outb %al,%edx
	jmp .+2
	movb %bh,%al
	incl %edx
	outb %al,%edx
	jmp .+2
	movl %ebx,(%edi)
	ret
//}}}
//{{{ function get_disp_pos
get_disp_pos:
	movl $KM_CURSOR,%edi
	movl (%edi),%eax
	shll $1,%eax
	ret
//}}}
//{{{ function init_env
init_env:
	xorl %eax,%eax
	movl %eax,%edi
	movl $0x1c00,%ecx		#zero 0~0x7000空间，准备安装idt,gdt
	rep stosl
	movl $KM_MEM,%edi
	movl (%edi),%eax
	cmpl $MEM_REQUEST,%eax
	jae 1f
	movl $0xfafa,%eax
	jmp .
1:
	andl $0xfff00000,%eax
	movl %eax,%ebx
#	shll $3,%ebx
	shrl $1,%ebx
	movl %ebx,kmem_size
	rorl $13,%eax
	decl %eax
	movl %eax,umem_begin
	ret
//}}}
//{{{ function crt_segdesc	建立一个段描述符
/*本函数可建立gdt/ldt所用的代码段、数据段描述符，以及系统段和门描述符
1、本函数只生成规定特权级的描述符，不会在GDT生成特权级为3的数据和代码段，不会在LDT中生成特权级0的代码和数据段；
对门的描述符同样有限制：不生成特权级0的陷阱门，特权级3的中断门以及特权级0的任务门。
本函数忽略调用门的生成。
2、本函数具有一定的错误检测能力：
（1）、对生成不同特权级的描述符的限制检测，
（2）、对参数的格式，描述符类型有效索引：0-6
（3）、对段界限引用的大小(最大不得超过物理内存,最小不能小于0xf字节)是否超出都可进行简单的检测
（4）、对基地址检测同段界限检测。
输入参数：
eax:	al:段类型；=0 数据段；=1 代码段；=2 ldt；=3 tss；=4 任务门；=5 中断门；=6 陷阱门；
		--0x9a, 0x92, 0xfa, 0xf2 ,0xe2, 0xe9, 0xe5, 0x8e, 0xef
		ah: =0 gdt；=1 ldt；
ebx:	bl:颗粒度；=0 byte；=1 4k。		 --0xc 0x4
ecx:	段限长(对于门是段选择符)； 低20位有效，还要结合颗粒度判断长度不超过实际内存大小
edx:	段基地址（对于门是入口地址）；
默认设置： 存在位=1；描述符类型：=1 (数据或代码段) =0 (系统段或门)	D/B位： =1 (32位代码段或数据段) =0 (系统段或门)
输出：
if  ecx == 0
			eax:edx 保存构造好的8字节段描述符，eax保存底4字节，edx保存高四字节.
else
 	        ecx == errorcode

为减少代码量使用下列变量减少分支判断：
stype0:	.byte	0x92,0x9a,0xe2,0xe9,0xe5,0x8e,0xef		#定义段类型  调用门0xec忽略
stype3:	.byte	0xf2,0xfa,0,0,0xe5,0,0
slim0:	.byte	0x40,0x40,0,0,0,0,0
slim1:	.byte	0xc0,0xc0,0x80,0,0,0,0					#ldt允许设置和使用颗粒度；
*/	
crt_segdesc:
	pushl %ebp
	movl %esp,%ebp
	pushl %eax
	pushl %ebx
	pushl %ecx
	pushl %edx
#先对所有的参数有效性进行检测，然后再组合成描述符 1、无须检测，自动生成。
#2、参数格式：描述符索引，gdt/ldt类型，颗粒度，段限长，段基地址，内存范围检测
	movl -4(%ebp),%eax
	cmpb $7,%al				#描述符格式
	jb 1f
	movl $0x1fff,%ecx
	jmp 9f
1:
	cmpb $2,%ah
	jb 2f					#表类型
	movl $0x1ffe,%ecx
	jmp 9f
2:
	movl -8(%ebp),%eax
	cmpb $2,%al				#颗粒度
	jb 3f
	movl $0x1eff,%ecx
	jmp 9f
3:
	movl -12(%ebp),%eax
	cmpl $0x100000,%eax		#段限长
	jb 4f
	movl $0x2fff,%ecx
	jmp 9f
4:
	cmpl $0xf,%eax			#下限，不得小于15字节
	jae 5f 
	movl -8(%ebp),%ebx
	cmpb $0,%bl				#颗粒度=1时允许
	jne 5f
	movl -4(%ebp),%ebx
	cmpb $3,%bl
	ja 5f
	movl $0x2ffe,%ecx
	jmp 9f
5:
	movl -16(%ebp),%eax
	movl $KM_MEM,%ebx
	cmpl (%ebx),%eax
	jb 1f
	movl $0x3fff,%ecx
	jmp 9f
#单项输入检测完成，开始内存范围检测，基地址+限长<物理内存,（限门以外的描述符）
1:
	movl -4(%ebp),%eax
	cmpb $3,%al
	ja 1f
	movl -8(%ebp),%eax
	movl -12(%ebp),%edx
	cmpb $0,%al
	je 2f
	shll $12,%edx
2:
	clc
	movl -16(%ebp),%eax
	adcl %edx,%eax
	jnc 3f
	movl $0x4fff,%ecx
	jmp 9f
3:
	movl $KM_MEM,%ebx
	cmpl (%ebx),%eax
	jb 1f
	movl $0x4ffe,%ecx
	jmp 9f
#范围检测完成，开始组合检测 ：如果是ldt,tss,getes的话：ldt选项错误，颗粒度无效。
#Note:但！任务门和ldt组合有效。
1:	
	movl -4(%ebp),%eax
	movl -8(%ebp),%ebx
	cmpb $0,%ah
	je 2f
	cmpb $2,%al
	jb 2f
	cmpb $4,%al
	je 2f
	movl $0x5fff,%ecx
	jmp 9f
2:							#wrong check over
	movl -4(%ebp),%eax
	cmpb $0,%ah
	jne 1f
	leal stype0,%esi
	jmp 2f
1:
	leal stype3,%esi
2:
	andl $0x7,%eax
	addl %eax,%esi
	lodsb
	movl %eax,%edx			#save in edx
	xorl %edi,%edi
	movl -8(%ebp),%ebx		#load ebx = granularity
	movl -4(%ebp),%eax
	cmpb $0,%bl
	jne 3f
	leal slim0,%esi
	movl $1,%edi
	jmp 4f
3:
	leal slim1,%esi
4:
	andl $7,%eax
	addl %eax,%esi
	lodsb
	movb %dl,%ah
	movw %ax,%dx			#now dl=G/DB/0AVL/limit/,dh=P/DPL/S/TYPE
	movl -12(%ebp),%eax
	movl -4(%ebp),%ebx
	cmpb $3,%bl
	ja 5f
	decl %eax
5:	
	rorl $16,%eax
	addl %eax,%edx			# limit in dl is available;low-16==>high16;edx is finished
	movl -16(%ebp),%eax
	xchgw %ax,%dx			#gate
	xchgl %eax,%edx			#gate finished
	movl -4(%ebp),%ebx
	cmpb $4,%bl
	jb 1f
	xorl %ecx,%ecx
	jmp 9f
1:
	bswap %edx
	rorl $8,%edx
	xorl %ecx,%ecx
	rorl $16,%eax
9:
	movl %ebp,%esp
	popl %ebp
	ret
//}}}
//{{{ function show_msg		测试函数，显示信息
#in: esi:	buffer address
#	 ecx:	buffer length
#	 eax:	=1 new line	
show_msg:
	push %es
	pushl %ecx
	pushl %esi
	cmpl $1,%eax
	jne 3f
	movl $KM_CURSOR,%edx
	movl (%edx),%eax
	movl $0,%ecx
	movl $0,%ebx
1:
	cmpl %ebx,%eax
	jbe 2f
	incl %ecx
	addl $80,%ebx
	jmp 1b
	cmpl $23,%ecx
	jb 2f
	call clsscr
	jmp 3f
2:
	movl %ebx,(%edx)
	movl $0,%eax
	call set_cur_pos
3:	
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $KM_CURSOR,%edx
	movl (%edx),%edi
	shll $1,%edi
	popl %esi
	popl %ecx
	pushl %ecx
	movb $0xc,%ah
4:	
	lodsb
	movb %al,%bl
	andb $0xf0,%al
	rorb $4,%al
	cmpb $9,%al
	jbe 1f
	addb $7,%al
1:
	addb $0x30,%al
	stosw
	movb %bl,%al
	andb $0xf,%al
	cmpb $9,%al
	jbe 2f
	addb $7,%al
2:
	addb $0x30,%al
	stosw
	movb $0x20,%al
	stosw
	loop 4b
	popl %ebx
	leal (%ebx,%ebx,2),%eax
	call set_cur_pos
	pop %es
	ret
//}}}
//{{{ function show_desc	测试显示建立的描述符，并且采用我熟悉的word方式显示
#in eax,edx	:	seg or gate descriptor
show_desc:
	pushl %edx
	pushl %eax
	movl $sbuff,%edi
	movl $2,%ecx
1:
	popl %eax
	xchgb %ah,%al
	stosb
	xchgb %ah,%al
	stosb
	bswap %eax
	stosb
	movb %ah,%al
	stosb
	loop 1b
/*2:	
	stosb
	rorl $8,%eax
	loop 2b
	decl %ebx
	cmpl $0,%ebx
	ja 1b */
	movl $sbuff,%esi
	movl $8,%ecx
	movl $1,%eax
	call show_msg
	ret
//}}}
//{{{ function show_01	全部为测试代码，测试显示函数和描述符的生成
show_01:
	movl $1,%eax
	movl $1,%ebx
	movl $0xf,%ecx
	movl $0x2000,%edx
	call crt_segdesc		# 0xe,0x2000,0x9a00,0x00c0
	cmpl $0,%ecx
	je 1f
	jmp .
1:
	call show_desc
	movl $0,%eax
	movl $1,%ebx
	movl $0x300,%ecx
	movl $0xb8000,%edx
	call crt_segdesc		#0x2ff,0x8000,0x920b,0x00c0
	cmpl $0,%ecx
	jne 2f
	call show_desc
	movl $0x101,%eax
	movl $1,%ebx
	movl $0x20,%ecx
	movl $0x123f000,%edx
	call crt_segdesc		#0x1f,0xf000,0xfa23,0x1c0
	cmpl $0,%ecx
	jne 2f
	call show_desc
	movl $0x100,%eax
	movl $0,%ebx
	movl $0x100,%ecx
	movl $0x995000,%edx
	call crt_segdesc		# 0x00ff,0x5000,0xf299,0x0040
	cmpl $0,%ecx
	jne 2f
	call show_desc
	movl $3,%eax
	movl $0,%ebx
	movl $104,%ecx
	movl $0x14000,%edx
	call crt_segdesc		# 0x0068,0x4000,0xe901,0x0000
	cmpl $0,%ecx
	jne 2f
	call show_desc
	movl $5,%eax
	movl $0,%ebx
	movl $0x50,%ecx
	movl $0xa000,%edx
	call crt_segdesc		#0x0050,0xa000,0x8e00,0x0000
	cmpl $0,%ecx
	jne 2f
	call show_desc
	movl umem_begin,%eax
2:	
	jmp .
	ret
//}}}
//{{{ function crt_idt	建立中断描述符表
crt_idt:
	pushl %ebp
	movl %esp,%ebp
	movl $GDT_DS,%ebx
	movw %bx,%es
	pushl $1
	pushl $0x170		#0x2e
	pushl $hdc_int
	pushl $1
	pushl $0x130
	pushl $flp_int
	pushl $1
	pushl $0x100
	pushl $time_int
	pushl $8
	pushl $0x140
	pushl $nor_8259s_int
	pushl $8
	pushl $0x100
	pushl $nor_8259m_int
	pushl $256
	pushl $0
	pushl $nor_int
1:	
	movl $5,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	popl %edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 9f
	popl %edi
	popl %ecx
2:
	stosl
	xchgl %eax,%edx
	stosl
	xchgl %eax,%edx
	loop 2b					# nor_int setup finished
	cmpl %esp,%ebp
	jne 1b					# everything can be loop ;-)
	movl $6,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	leal sys_int,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 9f
	movl $0x400,%edi
	stosl
	xchgl %eax,%edx
	stosl
9:
	movl %ebp,%esp
	popl %ebp
	ret
//}}}
//{{{ function crt_gdt	建立最终的gdt表
crt_gdt:
	pushl %ebp
	movl %esp,%ebp
	push %es
	movl $GDT_DS,%eax
	movw %ax,%es
	movl $1,%eax
	movl $1,%ebx
	movl umem_begin,%ecx
	incl %ecx
	movl $0,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 9f
	movl $GDT_BEG,%edi
	stosl
	xchgl %edx,%eax
	stosl					#0x8	text	1fff,0,0x9a00,0x00c0
	pushl %edi
	movl $0,%eax
	movl $1,%ebx
	movl umem_begin,%ecx
	incl %ecx
	movl $0,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 9f
	popl %edi
	stosl
	xchgl %eax,%edx
	stosl					#0x10	data	1fff,0,0x9200,0x00c0
	pushl %edi
	movl $0,%eax
	movl $1,%ebx
	movl $4,%ecx
	movl $STK0,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 9f
	popl %edi
	stosl
	xchgl %eax,%edx
	stosl					#0x18	stack	3,0xc000,0x9209,0x00c0
	pushl %edi
	movl $0,%eax
	movl $1,%ebx
	movl $2,%ecx
	movl $DISP_BASE,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 9f
	popl %edi
	stosl
	xchgl %eax,%edx
	stosl					#0x20	disp	1,0x8000,0x920b,0x00c0
	pop %es
9:
	movl %ebp,%esp
	popl %ebx
	ret
//}}}


//{{{ function show_02	测试函数，显示建立的中断描述符表格式
show_02:
	movl $0x100,%esi
	movl $16,%ecx
	movl $1,%eax
	call show_msg
	ret
//}}}


.org	4092
.ascii	"tian"
.include "asm/floppy.s"
.include "asm/athd.s"



stype0:	.byte	0x92,0x9a,0xe2,0xe9,0xe5,0x8e,0xef		#定义段类型  调用门0xec忽略
stype3:	.byte	0xf2,0xfa,0,0,0xe5,0,0
slim0:	.byte	0x40,0x40,0,0,0,0,0
slim1:	.byte	0xc0,0xc0,0x80,0,0,0,0					#ldt允许设置和使用颗粒度；
sbuff:	.space	64,0
kmem_size:	.long	0			#user address的起始地址
umem_begin:	.long	0			#段限长
m01:	.ascii	"test write to floppy!"
len=.-m01

//{{{	system const designed by below:
/*	系统表及数据的直观显示，便于查阅:
shadowgdt:	.word	0,0,0,0
			.word	0x3ff,0,0x9a00,0x00c0			#0x8	text
			.word	0x3ff,0,0x9200,0x00c0			#0x10	data
			.word	0xf,0,0x9230,0x00c0				#0x18	stack
			.word	1,0x8000,0x920b,0x00c0			#0x20	disp

shadowldt:	.word	0x3ff,0,0x9a40,0x00c0			#0x7	text
			.word	0x3ff,0,0x9240,0x00c0			#0xf	data
			.word	0xf,0,0x9280,0x00c0				#0x17	stack		8M以上的首个64k为堆栈 0x80ffff~~0x800000
			.word	1,0x8000,0x920b,0x00c0			#0x1f	disp

shadowtss:	.long	0								#task link
			.long	0xffff,0x18,0,0,0,0				#ss0:esp0,ss1:esp1,ss2:esp2
			.long	PD0,0,0,						#cr3,eip,eflags
			.long	0,0,0,0,0,0,0,0					#eax,ecx,edx,ebx,esp,ebp,esi,edi
			.long	0,0,0,0,0,0						#es,cs,ss,ds,fs,gs
			.long	0,0								#ldt,io-bitmap
shadowpdt:	.space 	4096,0
shadowidt:	.space	0x800,0
以64M内存为例，最终建立的系统表为：
64M=0x400,0000,32M=0x200,0000
gdt:		.word	0,0,0,0
			.word	0x3fff,0,0x9a00,0xc0			#0x8	text
			.word	0x3fff,0,0x9200,0xc0			#0x10	data
			.word	3,0xc000,0x9209,0x00c0			#0x18	stack	STK0=0x9c000
			.word	1,0x8000,0x920b,0x00c0			#0x20	disp
			.word	32,0x8a00,0xe200,0				#0x28	ldt
			.word	104,0,0x9212,0x0040				#0x30	tss0
			.word	0xf,0,0x92ff,0x1c0				#0x38	stack1
			.word	104,104,0x9212,0x0040			#0x40	tss1
			.word	0xf,0,0x92fe,0x1c0				#0x48	stack2
			.word	104,208,0x9212,0x0040			#0x50	tss2
			#....
	
ldt:		.word	0x3fff,0,0xfa00,0x2c0			#0xf	text
			.word	0x3fff,0,0xf200,0x2c0			#0x17	data
			.word	0xf,0,0xf2ff,0x3c0				#0x1f	stack 0x3ff0000
			.word	1,0x8000,0xf20b,0x00c0			#0x27	disp
			




idt_beg:	.long	0								#offset of idt		end:0x800
#gap:		0x100
gdt_beg:	.long	0x900							#offset of gdt	max (4095+1)*8	end: 0x8900
#gap:		0x100
ldt_beg:	.long	0x8a00							#offset of ldt	only one ldt; max 32*8=0x100 end:0x8b00
#gap:		0x500									#buffer of FDC return parameters	0x8c00
kernel:		.long 0x9000							#temporary kernel size=17*512=0x2200	end:	0xb200	--------------
#gap:		0xe00
link_tb:	.long	0xc000							#tss,pdt,pt link message table,4096*6=0x6000 end:0x12000
#gap:		0 no-gap
tss_beg:	.long	0x12000							#offset of first tss's block size=256*104=0x6800 end:0x18800
#gap:		0x800
pdt_beg:	.long	0x19000							#offset of first pdt's block size=64*4096=0x40000 end=0x59000
#gap:		0 no-gap
pt_beg:		.long	0x59000							#offset of first pt's block size=64*4096=0x40000  end=0x99000
#gap:		0
dma_beg:	.long	0x99000							#offset of dma buffer, size=18*512=0x2400		end=0x9b400					
#gap:		0xc00
buf_beg:	.long	0x9c000							#offset of template buffer size=0x4000			end 0xA0000
#gap:		0
disp_buf:	.long	0xA0000
*/
//}}}
.org	8700
.ascii	"yong"
#新扇区内容，用于磁盘驱动的读取测试
.ascii "jklmnopABCDEFG1234567890"
.org 9212
.ascii "TIAN"








