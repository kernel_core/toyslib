/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	一个通用的启动引导和内核初始化的头文件 
 *	本文件定义内存分配相关的常量
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifdef	_TOYS_MEM_TEMP
//{{{ 这里定义的是首次进入保护模式时，内存的分配状况.
#Note:这里的定义并没有真正使用，仅仅作为一个参照。在实模式下获取的参数表和内存大小直接拷贝至稳定形态下的位置了
TEXT_BASE			=	0					# offset=0x8000
TEXT_LIM			=	0x7ff				#8M
STK_BASE			=	0x80000				#				top of stack: 0x8ffff
//}}}
.endif
.ifdef	_TOYS_MEM_STAB
//{{{ 这里定义的是进入稳定形态的内核时，内存的分配状况
TEXT_BASE			=	0					#最终代码段起始地址
TEXT_LIM			=	0x3ff				#4M
STK_BASE			=	0x300000			#top of stack: 0x30ffff
STK1_BASE			=	0x310000
STK2_BASE			=	0x320000
STK3_BASE			=	0x330000
STK4_BASE			=	0x340000
KM_BASE				=	0x2f0000			#系统数据区：保存软驱参数，硬盘参数，光表位置，物理内存大小，软驱中断标志等
KM_LEN				=	0x500				#系统数据区长度，终止于0x2f0500
KM_FLPP				=	0x2f0000			#软驱参数的起始地址		12bytes
KM_HDP				=	0x2f0010			#硬盘参数的起始地址		16bytes
KM_CURSOR			=	0x2f0030			#光标位置				4bytes
KM_COUNTER			=	0x2f0038			#计数器位置				4bytes
KM_MEM				=	0x2f0040			#物理内存大小			4bytes
KM_FINT				=	0x2f0048			#软驱中断标志			4bytes
#硬盘驱动所用缓冲：	0x2f0050~~0x2f0070
KM_HDCMDIDX			=	0x2f0050			#硬盘的当前操作命令		4bytes
KM_HDCMDERR			=	0x2f0054			#硬盘命令错误后返回值	4bytes
KM_HDBUFF			=	0x2f0058			#保存硬盘读写缓冲区地址 4bytes
KM_HDLEFT			=	0x2f005c			#剩余的扇区读写次数		1byte

KM_HDPREC			=	0x2f0060			#写补偿					1byte
KM_HDSECC			=	0x2f0061			#读/写的扇区数			1byte
KM_HDSECB			=	0x2f0062			#扇区起始号(1-base)		1byte
KM_HDCYLL			=	0x2f0063			#16位柱面低8位			1byte
KM_HDCYLH			=	0x2f0064			#柱面号高8位			1byte
KM_HDDRVHD			=	0x2f0065			#0b101驱动器号/磁头号	1byte

KM_BUF				=	0x2f1000			#系统缓冲区
KM_BUF_LEN			=	0xf000				#系统缓冲区长度，60k
#KM_ORG				=	0x5500				#便于拷贝之前的系统数据
KM_FLPRT			=	0x8c00				#FDC返回参数缓冲区 base:0x8c00;size:0x50; HDD返回值：0x8c50
KM_HDRT				=	0x8c50
KM_FLPERR			=	0x8d00				#FDC错误代码存放;  HDD错误代码：0x8d50；size=0x20
KM_HDERR			=	0x8d50
#下面的位置暂时没有确定
GDT_BEG				=	0x900				#gdt 位置
GDT_LIM				=	0x7fff				#gdt描述符数量，最大4095个
IDT_BEG				=	0					#idt 位置
LDT_BEG				=	0x8a00				#ldt 位置，最大32个
LDT_LIM				=	0xff				#ldt描述符数量，最大32个
SYS_LINK			=	0x9000				#tss,pdt,pt表链信息，size=4096*9=0x9000
TSS_BEG				=	0x12000				#tss链中首个地址块，存放256*104字节的tss
PDT_BEG				=	0x19000				#pdt链中首个页目录表地址块，存放64个页目录表
PT_BEG				=	0x59000				#pt链中首个页表地址块，存放64个页表
STK0				=	0X9C000				#ring0级堆栈，稳定态内核最先使用的堆栈，size=16k; top=0x9ffff
//}}}
.endif
#下面是与运行时无关的定义:
STK_LIM				=	0xf					#stack's limit
DISP_BASE			=	0xb8000				#display's buffer
DISP_LIM			=	1					#8K
DMA_BASE			=	0x99000				#dma base;end of 0x9b400
DMA_LEN				=	0x2400				#dma最大缓冲区长度9k,对应软驱的一个track，18个扇区

