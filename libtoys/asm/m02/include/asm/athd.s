/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 * 	AT硬盘的驱动程序文件
 *	测试用镜像硬盘的参数：CHS=40*8*63=20160*512=10M
 *	--20230413对函数参数的传递方式进行调整，应为涉及中断函数调用，
 *	--同时硬盘所有命令参数具有一致性的特点，将参数放置在指定缓冲区中提供函数使用。	
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifndef	_TOYS_HDD_
_TOYS_HDD_	=	0
.endif
.include "defports.s"

//{{{hd_controller_reset	硬盘控制器的复位操作，当硬盘是非引导盘时，必须要先复位控制器，再执行参数建立函数才可正常操作
/*in:		none
  out:		CF clear if success
*/
hd_controller_reset:
#复位操作将写补偿字节取出
	movl $KM_HDP,%ebx
	movb 5(%ebx),%al
	shrb $2,%al
	movl $KM_HDPREC,%ebx
	movb %al,(%ebx)
	movl $4,%eax
	movl $HDP_CMD,%edx
	outb %al,%dx
	movl $10,%eax
	int $0x80
	call hd_check_status
	ret
//}}}
//{{{hd_check_status	硬盘操作的初始化函数：硬盘的控制器检测，命令发送，驱动器状态检测
/*in:		none 硬盘控制器命令字	0xc8 or 0x8 HD_CONTROL_BYTE
  out:		CF clear if success
*/
hd_check_status:
	movl $KM_HDERR,%ebx
	movl $20,%ecx
1:	
	movl $HDP_STAT,%edx
	inb %dx,%al
	testb $HDST_BUSY,%al
	jz 2f
	movl $4,%eax
	int $0x80
	loop 1b
	movl $0x1a0,(%ebx)
	stc
	jmp 9f
2:
	movl $HD_CONTROL_BYTE,%eax
	movl $HDP_CMD,%edx
	outb %al,%dx					#send control command : more than 8 heads;disable ECC;disable retry
	jmp .+2
	jmp .+2		
	movl $40,%ecx
3:
	movl $HDP_STAT,%edx
	inb %dx,%al
	jmp .+2
	testb $HDST_READY,%al
	jnz 4f
	movl $4,%eax
	int $0x80
	loop 3b
	movl $0x1a1,(%ebx)
	stc
	jmp 9f
4:
	clc
9:
	ret
//}}}
//{{{hd_get_paras	取得硬盘基本参数表
/*in:	al: index
  out:	al: para by index
 */	
hd_get_paras:
	pushl %esi
	movl $KM_HDP,%esi
	andl $0xf,%eax
	addl %eax,%esi
	lodsb
	popl %esi
	ret
//}}}
//{{{hd_ports_set	硬盘控制器的参数设置-----硬盘操作之前的参数设置函数
/*in:		：		0x1f1	写补偿，取自参数表，无需传入
  			al:		扇区数	0x1f2
			ah:		起始扇区号	0x1f3
			bl:		低字节cylinder号	0x1f4
			bh:		高字节cylinder号	0x1f5
			cl:		磁头号
			ch:		驱动器号；cx需要组合为端口0x1f6的参数 0b101dhhhh
			dl:		命令码，
  out:		CF clear if success
*/
hd_ports_set:
#frist,test KM_HDCMDIDX = 0? false return;true go on
	pushl %ebp
	movl %esp,%ebp
	pushl %eax
	pushl %ebx
	pushl %ecx
	pushl %edx
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $KM_HDCMDIDX,%ebx
	cmpl $0,(%ebx)					#参数设置之前，必须检查命令字是否为0,否则表示前次命令没有执行完
	je 1f
	movl $KM_HDERR,%ebx
	movl $0x1a2,(%ebx)
	stc
	jmp 9f
1:
	movl -16(%ebp),%eax
	cmpb $HDEXE_REST,%al
	jb 8f
	cmpb $HDEXE_SPEC,%al
	ja 8f
	andl $0xff,%eax
	movl %eax,(%ebx)					#save command
	movl $KM_HDPREC,%ebx
	movl $KM_HDP,%edx
	movw 5(%edx),%ax
	shrw $2,%ax
	movb %al,(%ebx)						#save precomp
	movl -4(%ebp),%eax
	movb 14(%edx),%cl
	addb %ah,%al
	jc 8f
	decb %al							#1-base;need dec 1 times
	cmpb %cl,%al						#起始扇区+扇区数>默认的扇区数
	ja 8f
	movl -4(%ebp),%eax
	cmpb $0,%al
	jbe 8f
	movb %al,1(%ebx)					#save sector count
	cmpb $0,%ah
	jbe 8f
	movb %ah,2(%ebx)					#save sector index number
	movl -8(%ebp),%eax
	movb %al,3(%ebx)					#save low  8 bits of cylinder
	movb %ah,4(%ebx)					#save high 8 bits of cylinder
	movl -12(%ebp),%eax
	andb $0xf,%al
	movb 2(%edx),%cl					#bios head count
	cmpb %cl,%al
	jae 8f
	andb $1,%ah
	rolb $4,%ah
	addb $0xa0,%ah
	addb %ah,%al						#format: 0b101dhhhh
	movb %al,5(%ebx)					#save driver & heads
	clc
	movl $KM_HDSECC,%ebx
	movl $KM_HDLEFT,%edx
	movb (%ebx),%al
	movb %al,(%edx)						#save sector counts
	jmp 9f
8:
	movl $KM_HDERR,%ebx
	movl $0x1a3,(%ebx)
	stc
9:
	pop %ds
	movl %ebp,%esp
	popl %ebp
	ret	
//}}}
//{{{hd_cmd_launch	 执行设定的硬盘操作命令
/*in:		none
  out:		CF clear if success
 */	
hd_cmd_launch:
	call hd_check_status
	jc 9f
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $KM_HDPREC,%esi
	movl $HDP_PRECOMP,%edx			#port 0x1f1
	lodsb							#finished shrb $2
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_SECTC,%edx			#port 0x1f2
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_SECTB,%edx			#port 0x1f3
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_CYLL,%edx				#port 0x1f4
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_CYLH,%edx				#port 0x1f5
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_DRVHD,%edx			#port 0x1f6
	outb %al,%dx
	jmp .+2
	movl $KM_HDCMDIDX,%esi
	lodsb
	movl $HDP_EXE,%edx				#port 0x1f7
	outb %al,%dx
	pop %ds
9:	
	ret
//}}}
//{{{hd_normal_int
hd_normal_int:
	movl $HDP_STAT,%edx
	inb %dx,%al
	testb $HDST_ERR,%al
	jz 1f
	movl $HDP_ERR,%edx
	inb %dx,%al
	movl $KM_HDRT,%ebx
	movb %al,(%ebx)
	stc
	jmp 9f
1:
	movl $100,%ecx
2:	
	movl $HDP_STAT,%edx
	inb %dx,%al
	testb $HDST_READY,%al
	jnz 3f
	jmp .+2
	jmp .+2
	loop 2b
	movl $KM_HDRT,%ebx
	movb $0xff,(%ebx)
	stc
	jmp 9f
3:
	clc
	xorl %eax,%eax
9:	
	ret
//}}}
//{{{hd_read_int	读硬盘操作
hd_read_int:
#	call hd_normal_int
#	jc 9f
	movl $256,%ecx
	movl $HDP_DATA,%edx
	movl $KM_HDBUFF,%edi
	rep insw
	movl $KM_HDLEFT,%ebx
	movb (%ebx),%al
	cmpb $1,%al
	ja 1f
	movl $KM_HDCMDIDX,%edx
	movl $0,(%edx)				#clear command 
	movb $1,%al
1:
	decb %al
	movb %al,(%ebx)
9:	
	ret
//}}}	
//{{{hd_write_int	写硬盘操作
hd_write_int:
#	call hd_normal_int
#	jc 9f
	movl $256,%ecx
	movl $HDP_DATA,%edx
	movl $KM_HDBUFF,%esi
	rep outsw
	movl $KM_HDLEFT,%ebx
	movb (%ebx),%al
	cmpb $1,%al
	ja 1f
	movl $KM_HDCMDIDX,%edx
	movl $0,(%edx)				#clear command 
	movb $1,%al
1:
	decb %al
	movb %al,(%ebx)
9:	
	ret
//}}}	

//{{{init_hd	test
init_hd:
	call hd_controller_reset
	jc 7f
	movl $KM_HDP,%esi
	movb 14(%esi),%al			#sector counts per cylinder
	movb $1,%ah					#first sector index 1-base
	movw (%esi),%bx				#cylinder
	xorl %ecx,%ecx
	movb 2(%esi),%cl			#head,driver
	decb %cl
	movl $HDEXE_SPEC,%edx
	call hd_ports_set
	jc 6f
	call hd_cmd_launch
	jc 8f
	movl $0x111111,%ecx
	jmp 9f
6:
	movl $0x765432,%ecx
	jmp 1f
7:
	movl $0x876543,%ecx
	jmp 1f
8:
	movl $0x987654,%ecx
1:
	movl $KM_HDERR,%ebx
	movl (%ebx),%eax
9:
	ret
//}}}	
//{{{do_write_to_hd			test! to write hd at 0 cylinder 0 head 1 sector
do_write_to_hd:
#	call hd_check_status
#	movl $0x456456,%ecx
#	jc 9f
	movl $0x9000,%eax
	addl $8704,%eax
	movl $KM_HDBUFF,%ebx
	movl %eax,(%ebx)
	movl $0x101,%eax		#1sector 1count
	movl $0,%ebx			#cylinder
	movl $0x1,%ecx			#driver,head
	movl $HDEXE_WRITE,%edx
	call hd_ports_set
	movl $0x128282,%ecx
	jc 9f
	call hd_cmd_launch
	movl $0xfa0001,%ecx
	jc 9f
	movl $0x10a999,%ecx
9:	
	ret
//}}}
//{{{do_seek		test
do_seek:
	movl $0x9000,%eax
	addl $8704,%eax
	movl $KM_HDBUFF,%ebx
	movl %eax,(%ebx)
	movl $0x101,%eax
	movl $0,%ebx
	movl $0,%ecx
	movl $HDEXE_SEEK,%edx
	call hd_ports_set
	movl $0x1900a,%ecx
	jc 9f
	call hd_cmd_launch
	movl $0x1900b,%ecx
	jc 9f
	movl $0x1900c,%ecx
9:
	ret
//}}}	
//{{{do_read_from_hd		test
do_read_from_hd:
	movl $KM_BUF,%eax
	movl $KM_HDBUFF,%ebx
	movl %eax,(%ebx)
	movl $0x101,%eax
	movl $0,%ebx
	movl $0,%ecx
	movl $HDEXE_READ,%edx
	call hd_ports_set
	movl $0x50aa01,%ecx
	jc 9f
	call hd_cmd_launch
	movl $0x50aa02,%ecx
	jc 9f
	movl $0x50aa00,%ecx
9:	
	ret
//}}}
//{{{do_restore_hd		test
do_restore_hd:
	movl $KM_BUF,%eax
	movl $KM_HDBUFF,%ebx
	movl %eax,(%ebx)
	movl $0x101,%eax
	movl $0,%ebx
	movl $0,%ecx
	movl $HDEXE_REST,%edx
	call hd_ports_set
	movl $0x60bb01,%ecx
	jc 9f
	call hd_cmd_launch
	movl $0x60bb02,%ecx
	jc 9f
	movl $0x60bb00,%ecx
9:	
	ret
//}}}
//{{{do_init_hd		test
do_init_hd:
	movl $KM_BUF,%eax
	movl $KM_HDBUFF,%ebx
	movl %eax,(%ebx)
	movl $0x13f,%eax
	movl $39,%ebx
	movl $7,%ecx
	movl $HDEXE_INIT,%edx
	call hd_ports_set
	movl $0x60cc01,%ecx
	jc 9f
	call hd_cmd_launch
	movl $0x60cc02,%ecx
	jc 9f
	movl $0x60cc00,%ecx
9:
	ret
//}}}	
//{{{do_verf_hd		test
do_verf_hd:
	movl $KM_BUF,%eax
	movl $KM_HDBUFF,%ebx
	movl %eax,(%ebx)
	movl $0x101,%eax
	movl $0,%ebx
	movl $0,%ecx
	movl $HDEXE_VERIFY,%edx
	call hd_ports_set
	movl $0x60dd01,%ecx
	jc 9f
	call hd_cmd_launch
	movl $0x60dd02,%ecx
	jc 9f
	movl $0x60dd00,%ecx
9:	
	ret
//}}}	
//{{{do_diag_hd		test
do_diag_hd:
	movl $KM_BUF,%eax
	movl $KM_HDBUFF,%ebx
	movl %eax,(%ebx)
	movl $0x13f,%eax
	movl $39,%ebx
	movl $7,%ecx
	movl $HDEXE_DIAG,%edx
	call hd_ports_set
	movl $0x60cc01,%ecx
	jc 9f
	call hd_cmd_launch
	movl $0x60cc02,%ecx
	jc 9f
	movl $0x60cc00,%ecx
9:
	ret
//}}}
//{{{do_format_hd	test
do_format_hd:
	movl $KM_BUF,%eax
	movl $KM_HDBUFF,%ebx
	movl %eax,(%ebx)
	movl $0x101,%eax
	movl $0,%ebx
	movl $0,%ecx
	movl $HDEXE_FORMAT,%edx
	call hd_ports_set
	movl $0x60dd01,%ecx
	jc 9f
	call hd_cmd_launch
	movl $0x60dd02,%ecx
	jc 9f
	movl $0x60dd00,%ecx
9:	
	ret
//}}}
















