/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 * 	软盘驱动器的驱动程序文件
 *
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifndef	_TOYS_8237A_
_TOYS_8237A_	=	0
_TOYS_FDC_		=	1
.endif
.include "defports.s"
#DMA0_STATUS_REG					=			0x08		#状态寄存器端口-只读(in)

//{{{ setup_dma		设置dma写工作方式和状态		读取磁盘数据
/* in:		eax=buffer address,DMA_BASE=0X99000;	DMA_LEN=0x2400
   			ecx=sector counts,扇区数
			edx=0		读扇区；  =1	写入扇区
   out:		cf=0 success;cf=1 error
 */
setup_dma:
	pusha
	cli
	clc
	movl %eax,%ebx
	andl $0xfffff,%ebx
	movl $6,%eax					#单通道屏蔽命令，屏蔽通道2，准备通道2设置
	outb %al,$DMA0_CHANMASK_REG		#单通道屏蔽寄存器端口
	jmp .+2
	outb %al,$DMA0_CLEARBYTE_REG	#清字节指针寄存器
	jmp .+2
	cmpl $0,%edx
	jne 1f
	movl $0x46,%eax					#模式寄存器命令字，通道2，写入内存，不自动，单字节传输
	jmp 2f
1:
	movl $0x4a,%eax					#模式寄存器命令字，通道2，写入磁盘，不自动，单字节传输
2:	
	outb %al,$DMA0_MODE_REG			#模式寄存器端口
	jmp .+2
	outb %al,$DMA0_CLEARBYTE_REG	#清字节指针寄存器
	jmp .+2
	movl %ebx,%eax				#dma buffer
	rorl $16,%eax					#get high 4 bits
	outb %al,$DMA_PAGE_CHAN2_ADDRBYTE2	#dma页寄存器端口
	jmp .+2
	outb %al,$DMA0_CLEARBYTE_REG	#清字节指针寄存器
	jmp .+2
	movl %ebx,%eax					#准备发送低8位地址
	outb %al,$DMA0_CHAN2_ADDR_REG	#低地址寄存器端口
	jmp .+2
	movb %ah,%al
	outb %al,$DMA0_CHAN2_ADDR_REG	#低地址寄存器端口
	jmp .+2
	movl %ecx,%eax
	cmpl $18,%eax
	jbe 1f
	stc
	jmp 9f
1:
	movl $512,%ecx
	mul %cx
	outb %al,$DMA0_CHAN2_COUNT_REG	#计数寄存器端口
	jmp .+2
	movb %ah,%al
	outb %al,$DMA0_CHAN2_COUNT_REG	#计数寄存器端口
	jmp .+2
	movl $2,%eax
	outb %al,$DMA0_CHANMASK_REG		#单通道屏蔽寄存器端口,解除屏蔽
	jmp .+2
9:
	sti
	popa
	ret
//}}}
//*******下面定义保护模式下的软驱驱动程序*************
//{{{delay	软驱驱动所用的延时函数
#in		al:延时次数，单位为1/100秒
#out	none
delay:
	andl $0xff,%eax
	int $0x80
	ret
//}}}	
//{{{motor_on	软驱马达的启动函数
motor_on:
	pushl %eax
	movl $0x1c,%eax				#driverA,控制允许，DMA模式，马达启动
	movl $FLPYDSK_DOR,%edx
	outb %al,%dx
	movl $40,%eax
	call delay
	popl %eax
	ret
//}}}
//{{{motor_off	软驱马达的关闭函数
motor_off:
	pushl %eax
	movl $4,%eax				#driverA,控制允许，马达关闭
	movl $FLPYDSK_DOR,%edx
	outb %al,%dx
	movl $40,%eax
	call delay
	popl %eax
	ret
//}}}	
//{{{fetch_param	软驱bios参数的获取函数
/*in:		al:待获取参数的索引
  out:		al:取得的参数
*/
fetch_param:
	pushl %esi
	movl $KM_FLPP,%esi
	andl $0xf,%eax
	addl %eax,%esi
	lodsb
	popl %esi
	ret
//}}}	
//{{{wait_for_irq	软驱的中断响应等待函数
/*in:		none
  out:		CF clear if success
  Note:	 whenever a read, write, seek, or calibrate command completes as well as during initialization
*/	
wait_for_irq:
	pushl %eax
	pushl %ebx
	pushl %ecx
	movl $80,%ecx
	movl $KM_FINT,%ebx
1:
	movl (%ebx),%eax
	test $0x80,%eax
	jnz 2f
	movl $20,%eax
	call delay
	loop 1b
	stc
	jmp 3f
2:
	movl $0,%eax
	movl %eax,(%ebx)
	clc
3:	
	popl %ecx
	popl %ebx
	popl %eax
	ret
//}}}	
//{{{send_cmd	软驱的FDC命令或参数发送函数
/*in:		al	command or parameter where be launch
  out:		CF clear is success
*/
send_cmd:
	pusha
	movl %eax,%ebx
	movl $FLPYDSK_MSR,%edx		#main status register 主状态寄存器
	movl $4,%ecx
1:
	inb %dx,%al
	jmp .+2
	jmp .+2
	testb $FLPYDSK_MSR_MASK_DATAREG,%al		#data register ready
	jnz	2f
	movl $10,%eax
	call delay
	loop 1b
	stc
	jmp 3f
2:
	movl %ebx,%eax
	movl $FLPYDSK_FIFO,%edx			#data register	数据寄存器
	outb %al,%dx
	jmp .+2
	clc
3:
	popa
	ret
//}}}
//{{{recv_cmd	软驱的FDC执行状态（返回字）取得函数
/*in:		eax=parameters count
  out:		CF clear if success
 */
recv_cmd:
	pusha
	push %es
	movl $GDT_DS,%ebx
	movw %bx,%es
	clc
	movl %eax,%ebx
	movl $KM_FLPRT,%edi
	movl $0,%eax
	movl $10,%ecx
	rep stosb
	movl $KM_FLPRT,%edi
#	movl $0,%ebx
	movl $4,%ecx
	movl $FLPYDSK_MSR,%edx
1:
	inb %dx,%al
	jmp .+2
	jmp .+2
#	test $FLPYDSK_MSR_MASK_BUSY,%al
	test $FLPYDSK_MSR_MASK_DATAREG,%al
	jnz 2f
	movl $10,%eax
	call delay
	loop 1b
	stc
	jmp 3f			#并不是所有的命令都返回7个参数
2:
	movl $FLPYDSK_FIFO,%edx		#data register
	inb %dx,%al
	jmp .+2
	stosb
#	incl %ebx
	decl %ebx
#	cmpl $8,%ebx
	cmpl $0,%ebx
	jbe 3f
	movl $FLPYDSK_MSR,%edx
	movl $4,%ecx
	jmp 1b
3:	
	pop %es
	popa
	ret
//}}}	
//{{{cmd_chk_interrupt	软驱的中断检查函数
/*out:	CF clear if success
  return parameters: 2
*/
cmd_chk_interrupt:
	pusha
	clc
	movl $FDC_CMD_CHECK_INT,%eax
	call send_cmd
	jnc 1f
	movl $KM_FLPERR,%ebx
	movl $0xe001,%eax
	movl %eax,4(%ebx)
	jmp 2f
1:
	movl $2,%eax		#2 byte return
	call recv_cmd
	jnc 2f
	movl $0xe002,%eax
	movl $KM_FLPERR,%ebx
	movl %eax,4(%ebx)
2:	
	popa
	ret
//}}}	
//{{{cmd_seek_head	软驱的磁头寻道定位函数
/*in:	al=header;(0,1)
  		ah=driver;(00,01,10,11)
		bx=cylinder(0~79)
  out:	CF clear if success		
  return parameters: none
 */
cmd_seek_head:
	pusha
	clc
	andb $4,%al
	andb $3,%ah
	addb %ah,%al
	movl $KM_FLPERR,%esi
	movl %eax,%ecx
	cmpw $80,%bx
	jb 1f
	movl $0xff01,%eax
	movl %eax,(%esi)
	stc
	jmp 9f
1:	
	movl $FDC_CMD_SEEK,%eax			#command 0xf seek/park head
	call send_cmd
	jnc 1f
	movl $0xf001,%eax
	movl %eax,(%esi)
	jmp 9f
1:
	movl %ecx,%eax					#parameter 1 xxxxx head hd1 hd0
	call send_cmd
	jnc 2f
	movl $0xf002,%eax
	movl %eax,(%esi)
	jmp 9f
2:
	movl %ebx,%eax					#parameter 1 Cylinder index
	call send_cmd
	jnc 3f
	movl $0xf003,%eax
	movl %eax,(%esi)
	jmp 9f
3:
	call wait_for_irq
	jnc 4f
	movl $0xf004,%eax
	movl %eax,(%esi)
	jmp 9f
4:
	call cmd_chk_interrupt
	jnc 9f
	movl $0xf005,%eax
	movl %eax,(%esi)
9:
	popa
	ret
//}}}
//{{{cmd_trans_sector	软驱的读/写磁盘扇区函数
/*in:		al=xxxx HD DR1 DR0	HD=head DR1 DR0=driver(00,01,10,11)
  			ah=Cylinder
			bl=head
			bh=sector number(1-base)
			cx=0 read;=1 write srctor
			Other parameters fetch flp-parameters to get
  out:		CF clear if success
  return:	7 parameters
*/
cmd_trans_sector:
	pusha
	movl $KM_FLPERR,%esi
	andl $0xffff,%ecx
	cmpw $0,%cx
	jne 1f
	movl $FDC_CMD_EXT_SKIP,%ecx
	addl $FDC_CMD_EXT_DENSITY,%ecx
	addl $FDC_CMD_READ_SECT,%ecx	#command: FDC_CMD_READ_SECT | FDC_CMD_EXT_SKIP | FDC_CMD_EXT_DENSITY
	jmp 2f
1:
	movl $FDC_CMD_EXT_DENSITY,%ecx
	addl $FDC_CMD_WRITE_SECT,%ecx	#command: FDC_CMD_WRITE_SECT | FDC_CMD_EXT_DENSITY
2:
	xchgl %eax,%ecx
	call send_cmd					#send command
	jnc 1f
	movl $0xd001,%eax
	movl %eax,(%esi)
	jmp 9f
1:
	movl %ecx,%eax
	andl $0xff,%eax
	call send_cmd					#send para 1  xxxx HD DR1 DR0
	jnc 2f
	movl $0xd002,%eax
	movl %eax,(%esi)
	jmp 9f
2:
	movl %ecx,%eax
	movb %ah,%al
	andl $0xff,%eax
	call send_cmd					#send para 2 Cylinder
	jnc 3f
	movl $0xd003,%eax
	movl %eax,(%esi)
	jmp 9f
3:
	movl %ebx,%eax
	andl $0xff,%eax
	call send_cmd					#send para 3 head
	jnc 4f
	movl $0xd004,%eax
	movl %eax,(%esi)
	jmp 9f
4:
	movl %ebx,%eax
	movb %ah,%al
	andl $0xff,%eax
	call send_cmd					#send para 4 sector index(1-base)
	jnc 5f
	movl $0xd005,%eax
	movl %eax,(%esi)
	jmp 9f
5:
	movl $3,%eax					#3 is index of floppy's bios parameters; means : sector size
	call fetch_param
	andl $0xff,%eax
	call send_cmd					#send para 5 sector size
	jnc 6f
	movl $0xd006,%eax
	movl %eax,(%esi)
	jmp 9f
6:
	movl $4,%eax					#4 is index of floppy's bios parameters; means : track length
	call fetch_param
	andl $0xff,%eax
	call send_cmd					#send para 6  track length
	jnc 7f
	movl $0xd007,(%esi)				#test
	jmp 9f
7:
	movl $5,%eax					#5 is index of floppy's bios parameters; means : length of GAP3
	call fetch_param
	andl $0xff,%eax
	call send_cmd					#send para 7  GAP3's length
	jnc 8f
	movl $0xd008,(%esi)
8:	
	movl $6,%eax					#6 is index of floppy's bios parameters; means : data length
	call fetch_param
	andl $0xff,%eax
	call send_cmd					#send para 8 data length
	jnc 1f
	movl $0xd009,(%esi)
	jmp 9f
1:
	call wait_for_irq
	jnc 2f
	movl $0xd00a,(%esi)
	jmp 9f
2:
	movl $7,%eax					#return 7 times
	call recv_cmd
	jnc 3f
	movl $0xd00b,(%esi)
	jmp 9f
3:
	call cmd_chk_interrupt
	jnc 9f
	movl $0xd00c,(%esi)
9:
	popa
	ret
//}}}
//{{{write_sector	软驱的写磁盘扇区函数
/*in:		al=xxxx HD DR1 DR0	HD=head DR1 DR0=driver(00,01,10,11)
  			ah=Cylinder
			bl=head
			bh=sector number(1-base)
			cx=sector counts,扇区数
			Other parameters fetch flp-parameters to get
  out:		CF clear if success
  return:	7 parameters
*/
write_sector:
	movl $KM_FLPERR,%esi
	addl $4,%esi
	movl %eax,%edi			# seek head used 
	pusha
	movl $DMA_BASE,%eax
	movl $1,%edx
	call setup_dma
	jnc 2f
	movl $0xda00,(%esi)
	popa
	ret
2:
	call motor_on
	movl %edi,%eax
	andl $0x3,%eax
	movb %al,%ah		#cmd_seek_head para2
	movl %edi,%ebx
	shrl $2,%ebx
	andl $1,%ebx
	movb %bl,%al		#cmd_seek_head para1
	movl %edi,%ebx
	xchgb %bh,%bl
	andl $0xff,%ebx		#cmd_seek_head para3
	call cmd_seek_head
	jnc 3f
	call motor_off
	movl $0x9f0a,(%esi)
	popa
	ret
3:	
	popa
	movl $1,%ecx
	call cmd_trans_sector
	jnc 4f
	movl $KM_FLPERR,%esi
	movl $0x9f0b,4(%esi)
4:	
	call motor_off
	ret
//}}}	
//{{{read_sector	软驱的读磁盘扇区函数
/*in:		al=xxxx HD DR1 DR0	HD=head DR1 DR0=driver(00,01,10,11)
  			ah=Cylinder
			bl=head
			bh=sector number(1-base)
			cx=sector counts,扇区数
			Other parameters fetch flp-parameters to get
  out:		CF clear if success
  return:	7 parameters
*/
read_sector:
	movl $KM_FLPERR,%esi
	addl $4,%esi
	movl %eax,%edi
	pusha
	movl $DMA_BASE,%eax
	movl $0,%edx			#read from floppy
	call setup_dma
	jnc 2f
	movl $0xda01,(%esi)
	popa
	ret
2:
	call motor_on
	movl %edi,%eax
	andl $3,%eax
	movb %al,%ah		#cmd_seek_head	para2
	movl %edi,%ebx
	shrl $2,%ebx
	andl $1,%ebx
	movb %bl,%al		#cmd_seek_head	para1
	movl %edi,%ebx
	xchgb %bh,%bl
	andl $0xff,%ebx		#cmd_seek_head	para3
	call cmd_seek_head
	jnc 3f
	call motor_off
	movl $0x9e0a,(%esi)
	popa
	ret
3:
	popa
	movl $0,%ecx		#read sector
	call cmd_trans_sector
	jnc 4f
	movl $KM_FLPERR,%esi
	movl $0x9e0b,4(%esi)
4:
	call motor_off
	ret
//}}}







