/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	一个通用的启动引导和内核初始化的头文件 
 *
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */

.ifdef	_TOYS_16_
//{{{code16 const
.code16
SMAP			=	0x534d4150
BOOTSEG			=	0x7c0
BOOTADDR		=	0x7c00
DISPSEG			=	0xb800
STKSEG			=	0x2000
STKSP			=	0x3ff
BUFFADDR		=	0x200

#==========8253
FRQ_8253		=	11930
CMD_8253		=	0x36
CMD_PORT_8253	=	0x43
DAT_LCK0_8253	=	0x40
#==========8259a
MASTER_A00_PORT	=	0x20					#主芯片端口 a0=0，用于发送icw1，ocw2
MASTER_A01_PORT	=	0x21					#主芯片端口 a0=1，用于发送icw2，icw3，icw4，ocw1
SLAVE_A00_PORT	=	0xa0					#从芯片端口 a0=0，用于发送icw1，ocw2
SLAVE_A01_PORT	=	0xa1					#从芯片端口 a0=1，用于发送icw2，icw3，icw4，ocw1
ICW1			=	0x11					#icw1命令：边沿触发中断，多片级联，需要icw4
ICW2_MASTER		=	0x20					#icw2命令：设定主芯片的中断处理起始号：0x20
ICW2_SLAVE		=	0x28					#icw2命令：设定从芯片的中断处理起始号：0x28
ICW3_MASTER		=	4						#icw3命令：设定级联端口，主芯片：4
ICW3_SLAVE		=	2						#icw3命令：设定级联端口，从芯片：2
ICW4			=	1						#icw4命令：设定工作模式：普通全嵌套，非缓冲，非自动结束，8086模式
OCW1_MASK		=	0xff					#ocw1命令：中断屏蔽
OCW1_UNMASK		=	0						#ocw1命令：中断非屏蔽
OCW2			=	0x20					#ocw2命令：非自动结束方式，发送EIO通知中断处理完成
//}}}
.else
//{{{code32 const
.code32
#type of segment
G_TEXT			=	0x9a				#GDT 代码段
G_DATA			=	0x92				#GDT 数据段
G_LDT			=	0xe2				#GDT 系统段LDT	特权级3
G_TSS			=	0xe9				#GDT 系统段TSS	特权级3
L_TEXT			=	0xfa				#LDT 代码段
L_DATA			=	0xf2				#LDT 数据段
GATE_INT		=	0x8e00				#GATE 中断门	特权级0
GATE_TRAP		=	0xef00				#GATE 陷阱门	特权级3
GATE_TASK		=	0xe500				#GATE 任务门	特权级3	;0x85 特权级0
GATE_CALL		=	0xec00				#GATE 调用门	特权级3；0x8c 特权级0
GRAN_SEG_4K		=	0xc0				#代码，数据段的颗粒度 4k
GRAN_SEG_1B		=	0x40				#代码，数据段的颗粒度 1byte
GRAN_GT_4K		=	0x80				#门，系统段的限长颗粒度 4k
GRAN_GT_1B		=	0					#门，系统段的限长颗粒度 1byte
# GDT segment selecetor
GDT_CS			=	0x8					#text
GDT_DS			=	0x10				#data
GDT_SS			=	0x18				#stack
GDT_DISP		=	0x20				#disp
GDT_SS1			=	0x28				#task1 kernel ss
GDT_SS2			=	0x30				#task2 kernel ss
GDT_SS3			=	0x38				#task3 kernel ss
GDT_SS4			=	0x40				#task4 kernel ss
GDT_LDT			=	0x48				#ldt
GDT_TSS0		=	0x50				#tss0
GDT_TSS1		=	0x58				#tss1
GDT_TSS2		=	0x60				#tss2
# LDT segment selector
LDT_CS			=	0xf					#text
LDT_DS			=	0x17				#data
LDT_SS			=	0x1f				#stack
LDT_DISP		=	0x27				#disp

//}}}
.endif
MEM_REQUEST		=	0x800000		#memory request at least 8M

.ifdef	_TOYS_KERN_
BUFS			=	0x101000		#temp buffer


.endif
