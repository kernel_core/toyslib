/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	toys进入稳定保护模式的head代码，完成idt,ldt,tss, pd, pdt系统表的设置安装，中断处理函数的代码实现及安装
 *	各类驱动的安装。最后跳转至c代码开始实现的内核。
 *
 *	20230429 测试任务启动： 1、页表测试正确
 *							2、ldt测试正确
 *							3、tss测试
 *
 *	fetch_raw_descriptor: GDT: index (157) 2a > limit (7f) --solved! 原因是构建ldt时没预留第一个空描述符！'^_^!
 *	20230503 任务已经顺利启动了，下一步是完善内存管理和页表设置。
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifndef _TOYS_MEM_STAB
_TOYS_MEM_STAB	=	0
.endif
.ifndef _TOYS_8259A_
_TOYS_8259A_	=	1
.endif
.include "defconst.s"
.include "deferrno.s"
.include "defmem.s"
.include "defports.s"

.text
	movl $GDT_DS,%eax
	movw %ax,%ds
	movw %ax,%es
	movw %ax,%fs
	movw %ax,%gs
	movl $STK_LIM,%eax
	shll $12,%eax
	decl %eax
	movl $KM_BUF,%edi
	movl %eax,(%edi)
	movl $GDT_SS,4(%edi)
	lss (%edi),%esp
	movl $GDT_LDT,%eax
	lldt %ax					#load ldt
	call crt_pd
	movl $_err_crt,%eax
	jc 9f
	call crt_upd
	jc 9f
	movl $PDT_BEG,%eax
	movl %eax,%cr3
	movl %cr0,%eax
	orl $0x80000000,%eax
	movl %eax,%cr0
	jmp $GDT_CS,$1f				#开启分页
1:
	xorl %eax,%eax
	call crt_tss
	movl $GDT_TSS0,%eax
	ltr %ax
#测试证明：除了任务状态段描述符的B位置位，任务状态段到目前未改变	
	call crt_idt
	jc 9f
	movl $KM_BUF,%edi
	movl $0x7ff,%eax
	movw %ax,(%edi)
	movl $IDT_BEG,%eax
	movl %eax,2(%edi)
	lidt (%edi)
	movl $OCW1_UNMASK,%eax
	outb %al,$MASTER_A01_PORT
	jmp .+2
	outb %al,$SLAVE_A01_PORT
	jmp .+2
	sti
	movl $12,%eax
	int $0x80
	movl $KM_COUNTER,%ebx
	movl (%ebx),%eax
	call init_mbitmap
#测试将位图信息写入硬盘镜像
	movl %esp,%ebp
	movl $HDEXE_READ,%eax			#参数7，操作命令-读取硬盘，加载任务代码
	pushl %eax
	pushl $0x2000000				#参数6，task1加载地址
	pushl $0						#驱动器号，默认0；第一个硬盘
	pushl $0						#磁头号（0-base）
	pushl $0						#柱面号（0-base）
	pushl $19						#起始扇区号(1-base)，测试用，写入当前内核数据之后的空扇区：0柱0头19扇区（chs）
	pushl $1						#写入的扇区数
	call do_hd_request
	movl $4,%eax
	int $0x80
	nop
	nop
#	movl $LDT_TSS1,%eax
#	call *%eax						#测试，无法直接调用ldt的任务门实现特权级的转移
	call start_task
	movl $KM_HDBUFF,%ebx
	movl (%ebx),%eax
	movl %ebp,%esp
9:	
	jmp .

//{{{function normal_int		通用的中断处理函数
normal_int:
	pusha
	push %es
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $462,%edi
	movl $0xb41,%eax
	stosw
	pop %es
	popa
	iret
//}}}
//{{{ function nor_8259m_int	通用的8259主芯片中断处理函数
nor_8259m_int:
	pusha
	push %es
	movl $0x20,%eax
	outb %al,$MASTER_A00_PORT
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $464,%edi
	movl $0xb42,%eax
	stosw
	pop %es
	popa
	iret
//}}}
//{{{function nor_8259s_int		通用的8259从芯片中断处理函数
nor_8259s_int:
	pusha
	push %es
	movl $0x20,%eax
	outb %al,$SLAVE_A00_PORT
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $466,%edi
	movl $0xc43,%eax
	stosw
	movl $0x20,%eax
	outb %al,$MASTER_A00_PORT
	pop %es
	popa
	iret
//}}}
//{{{ function time_int		8253定时器中断	int 0x20
time_int:
	pusha
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $KM_COUNTER,%ebx
	movl (%ebx),%eax
	incl %eax
	movl %eax,(%ebx)
	movl $OCW2,%eax
	outb %al,$MASTER_A00_PORT
	pop %ds
	popa
	iret
//}}}
//{{{ function floppy_int	软驱中断	int 0x26
floppy_int:
	pusha
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $KM_FINT,%ebx
	movl (%ebx),%eax
	orl $0x80,%eax
	movl %eax,(%ebx)
	movl $OCW2,%eax
	outb %al,$MASTER_A00_PORT
	pop %ds
	popa
	iret
//}}}	
//{{{ function hd_int	硬盘中断	int 0x2e
hd_int:
	pusha
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $OCW2,%eax
	outb %al,$SLAVE_A00_PORT
	call hd_normal_int
	movl $0xa45,%ecx
	jc 9f
	movl $KM_HDCMDIDX,%ebx
	cmpl $0,(%ebx)
	jne 1f
	movl $0xa46,%ecx
	jmp 9f
1:
	cmpl $HDEXE_REST,(%ebx)		#reset		a
	jne 2f
	movl $0,(%ebx)				#clear command bytes
	movl $0xb61,%ecx			#echo 'a' for rest
	jmp 9f
2:
	cmpl $HDEXE_READ,(%ebx)		#read		b
	jne 3f
	call hd_read_int
	movl $0xb62,%ecx
	jmp 9f
3:
	cmpl $HDEXE_WRITE,(%ebx)	#write		c
	jne 4f
	call hd_write_int
	movl $0xb63,%ecx
	jmp 9f
4:
	cmpl $HDEXE_VERIFY,(%ebx)	#verify		d
	jne 5f
	movl $0xb64,%ecx
	movl $0,(%ebx)
	jmp 9f
5:
	cmpl $HDEXE_FORMAT,(%ebx)	#format		e
	jne 6f
	movl $0xb65,%ecx
	movl $0,(%ebx)
	jmp 9f
6:
	cmpl $HDEXE_INIT,(%ebx)		#init		f
	jne 7f
	movl $0xb66,%ecx
	movl $0,(%ebx)
	jmp 9f
7:
	cmpl $HDEXE_SEEK,(%ebx)		#seek		g
	jne 1f
	movl $0xb67,%ecx
	movl $0,(%ebx)
	jmp 9f
1:
	cmpl $HDEXE_DIAG,(%ebx)		#diag		h
	jne 2f
	movl $0xb68,%ecx
	movl $0,(%ebx)
	jmp 9f
2:
	cmpl $HDEXE_SPEC,(%ebx)		#spec		i
	jne 3f
	movl $0xb69,%ecx
	movl $0,(%ebx)
	jmp 9f
3:
	movl $0xb6a,%ecx			#			j
9:
	push %es
	movl $GDT_DISP,%eax
	movw %ax,%es
	movl $468,%edi
	movl %ecx,%eax
	stosw
	pop %es
	movl $OCW2,%eax
	outb %al,$MASTER_A00_PORT
	pop %ds
	popa
	iret
//}}}
//{{{ function sys_int	int 0x80中断处理函数
/*目前仅响应延时调用
  in:		ax=0	延时中断号
  			al:		延时数，最大255*1/100秒
  out:		none
*/	
sys_int:
	pusha
	push %ds
	movl $GDT_DS,%edx
	movw %dx,%ds
	cmpb $0,%ah
	jne 9f
	movl $KM_COUNTER,%ebx
	andl $0xff,%eax
	addl (%ebx),%eax
1:
	cmpl (%ebx),%eax
	nop
	nop
	ja 1b
9:
	pop %ds
	popa
	iret
//}}}

//{{{function crt_pd		页目录和页表的建立函数，本函数仅建立固定内核所用的最多127个页表
crt_pd:
	pushl %ebp
	movl %esp,%ebp
	movl $PDT_BEG,%edi
	pushl %edi
	movl $KM_MEM,%ebx		#calc how many pd need to create
	movl (%ebx),%ecx
	roll $10,%ecx			#kernel memory's pd;half of total memory
	movl %ecx,%ebx
	cmpl $PDT_MAX,%ecx		#pdt's count in fixed position must be less PDT_MAX
	jbe 1f
	stc
	jmp 9f
1:
	movl -4(%ebp),%eax
	orl $7,%eax
2:
	addl $0x1000,%eax		#after pd,pdt puts in a row
	stosl
	loop 2b					#finished pd for kernel
	movl $0,%eax
	stosl
	movl $PDT_BEG,%edi
	addl $0x1000,%edi
	roll $10,%ebx
	movl %ebx,%ecx
	movl $7,%eax
3:
	stosl
	addl $0x1000,%eax
	loop 3b					#finished pdt for kernel
	testl $0xfff,%edi
	jz 4f
	andl $0xfffff000,%edi
	addl $0x1000,%edi
4:	
	movl $0xaa50,%eax
	stosl					#0xaa50 is sign of end for kernel pd/pdt 
	clc
9:
	movl %ebp,%esp
	popl %ebp
	ret
//}}}
//{{{function crt_upd	ring3页目录和页表的建立函数
crt_upd:
	pushl %ebp
	movl %esp,%ebp
	movl $PDT_BEG,%esi
	movl $0,%ecx
1:	
	movl (%esi),%eax
	cmpl $0xaa50,%eax
	je 2f
	incl %ecx
	addl $0x1000,%esi
	cmpl $127,%ecx
	jae 8f
	jmp 1b
2:
	movl %esi,%edi			#location user's pd
	movl $KM_UPD,%ebx
	movl %esi,(%ebx)		#save location
	movl $KM_MEM,%ebx
	movl (%ebx),%ecx
	roll $9,%ecx			#count of usr's pdt
	movl %ecx,%ebx
	clc
	addl %ebx,%ecx			#calc kernel's pd/pdt + usr's pd/pdt
	addl $2,%ecx
	jc 9f
	cmpl $PDT_MAX,%ecx
	jbe 3f
	jmp 8f
3:
	movl %ebx,%ecx
	movl $7,%eax			#ring3
	addl %edi,%eax
4:
	addl $0x1000,%eax
	stosl
	loop 4b					#usr's pd created
	movl $0,%eax
	stosl
	movl $KM_UPD,%edx
	movl (%edx),%edi
	addl $0x1000,%edi
	movl %ebx,%ecx
	roll $10,%ecx
	movl $KM_MEM,%edx
	movl (%edx),%eax
	rorl $1,%eax			#physical memory address for user
	addl $7,%eax
5:
	stosl
	addl $0x1000,%eax
	loop 5b
	testl $0xfff,%edi
	jz 6f
	andl $0xfffff000,%edi
	addl $0x1000,%edi		#保证4k对齐的位置上写入结束标志
6:	
	movl $0xaa50,%eax
	stosl
	clc
	jmp 9f
8:
	stc
9:	
	movl %ebp,%esp
	popl %ebp
	ret
//}}}	
//{{{ function crt_tss	内核默认的任务状态段的建立函数
/*根据之前的测试，内核tss无需正常初始化，他是在任务产生中断时，？？？*/	
crt_tss:
	pushl %ebp
	movl %esp,%ebp
	movl $TSS_BEG,%edi
	xorl %eax,%eax
	stosl						#befor tss's segdesc
	movl $0xffff,%eax			#esp0
	stosl
	movl $GDT_SS0,%eax			#ss0
	stosl
	xorl %eax,%eax
	movl $4,%ecx
	rep stosl					#ss1,esp1,ss2,esp2	
	movl $PDT_BEG,%eax
	stosl						#PDBR
	movl $16,%ecx
	xorl %eax,%eax
	rep stosl					#eip,eflags,eax,ecx,edx,ebx,esp,ebp,esi,edi,es,cs,ss,ds,fs,gs
#	movl $GDT_CS,%eax
#	movl $GDT_LDT,%eax
	stosl						#LDT,this tss is used to kernel,so set cs
	movl $0,%eax
	stosl						#io bitmap,trap sign		---tss0 only for test, for kernel
	movl $TSS_BEG,%edi
	addl $TSS_LIM,%edi
	movl $0,%esi
	pushl $GDT_SS3
	pushl $GDT_SS2
	pushl $GDT_SS1
1:	
	xorl %eax,%eax
	stosl
	movl $STK_LIM,%eax
	shll $12,%eax
	decl %eax
	stosl						#esp0
	popl %eax
#	movl $GDT_SS1,%eax
	stosl						#ss0
	xorl %eax,%eax
	movl $4,%ecx
	rep stosl					#ss1,esp1,ss2,esp2
#	movl $KM_UPD,%ebx
#	movl (%ebx),%eax
	movl $PDT_BEG,%eax			#目前仅使用一个页目录
	stosl						#PDBR
	movl $16,%ecx
	xorl %eax,%eax
	rep stosl					#eip,eflags,eax,ecx,edx,ebx,esp,ebp,esi,edi,es,cs,ss,ds,fs,gs
	movl $GDT_LDT,%eax
	stosl						#LDT
	xorl %eax,%eax
	stosl						#T & io bitmap
	andl $0xffffff00,%edi
	addl $TSS_LIM,%edi
	incl %esi
	cmpl $3,%esi
	jb 1b
	movl %ebp,%esp
	popl %ebp	
	ret
//}}}
//{{{ function crt_idt	中断描述符表的建立函数
crt_idt:
	movl $IDT_BEG,%edi
	movl $5,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	leal normal_int,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	movl $256,%ecx
1:
	stosl
	xchgl %eax,%edx
	stosl
	xchgl %eax,%edx
	loop 1b						#normal_int
	movl $IDT_BEG,%edi
	addl $0x100,%edi			#int 0x20
	movl $5,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	leal nor_8259m_int,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	movl $8,%ecx
2:
	stosl
	xchgl %eax,%edx
	stosl
	xchgl %eax,%edx
	loop 2b						#setup nor_8259m_int
	movl $5,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	leal nor_8259s_int,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	movl $8,%ecx
3:
	stosl
	xchgl %eax,%edx
	stosl
	xchgl %eax,%edx
	loop 3b						#setup nor_8259s_int
	movl $IDT_BEG,%edi
	addl $0x100,%edi			#time_int
	movl $5,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	leal time_int,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#setup time_int
	movl $IDT_BEG,%edi
	addl $0x130,%edi			#IRQ6 floppy_int	0x26
	movl $5,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	leal floppy_int,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#setup floppy_int
	movl $IDT_BEG,%edi
	addl $0x170,%edi			#IRQ14 hd_int		0x2e
	movl $5,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	leal hd_int,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#setup hd_int
	movl $IDT_BEG,%edi
	addl $0x400,%edi			#int 0x80
	movl $6,%eax
	movl $0,%ebx
	movl $GDT_CS,%ecx
	leal sys_int,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#setup sys_int
	clc
	jmp 9f
8:
	stc
9:
	ret
//}}}

///{{{ function init_tss1		for test
/*these fields in tss struct need to change:
  					esp0		0x4
					ss0			0x8
					cr3			0x1c
					eip			0x20
					eflags		0x24
					esp			0x38
					cs			0x4c
					ss			0x50
					ds			0x54
					ldt			0x60
 */	
init_tss1:
	movl $TSS_BEG,%ebx
	addl $TSS_LIM,%ebx
	movl $STK_LIM,%eax
	shll $12,%eax
	decl %eax
	movl %eax,4(%ebx)			#esp0
	movl $GDT_SS1,8(%ebx)		#ss0
	movl $KM_UPD,%edx
	movl (%edx),%eax
	movl $PDT_BEG,0x1c(%ebx)
#	movl %eax,0x1c(%ebx)		#cr3
	movl $0,0x20(%ebx)			#eip
	pushfl
	popl %eax
	movl %eax,0x24(%ebx)		#eflags
	movl $USTK_LIM,%eax
	shll $12,%eax
	decl %eax
	movl %eax,0x38(%ebx)		#esp
	movl $LDT_CS,0x4c(%ebx)		#cs
	movl $LDT_DS,0x48(%ebx)		#es
	movl $LDT_SS,0x50(%ebx)		#ss
	movl $LDT_DS,0x54(%ebx)		#ds
	movl $GDT_LDT,0x60(%ebx)	#ldt
	ret
//}}}
//{{{ function start_task
start_task:
	movl $6,%eax
	int $0x80			#wait for load task1
/*	pushfl
	andl $0xffffbfff,(%esp)
	popfl
	pushl $0x1f
	pushl $0x3fff
	pushfl
	pushl $0xf
	pushl $0
	iret
*/
	call init_tss1
	ljmp $GDT_TSS1,$0	
	jmp .
	ret
//}}}	

.include "src/floppy.s"
.include "src/athd.s"
.include "src/kern.s"
.include "src/kmem.s"



.org 7164
.ascii "toys"

