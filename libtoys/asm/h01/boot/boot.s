/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	一个通用的启动引导和内核初始化的头文件 
 *
 *  Copyright (c) 2023-2025 tybitsfox <tybitsfox@126.com>
 */
_TOYS_16_	=	2023
.include "defconst.s"
.include "deferrno.s"
.text
	jmp $BOOTSEG,$go
go:
	mov %cs,%ax
	mov %ax,%ds
	mov %ax,%es
	mov $0x200,%bx
	movw $STKSP,(%bx)
	movw $STKSEG,2(%bx)
	lss (%bx),%sp
	movw $47,(%bx)
	mov $gdt,%ax
	add $BOOTADDR,%ax
	mov %ax,2(%bx)
	xor %ax,%ax
	mov %ax,4(%bx)
	lgdt (%bx)
	call check_set_vmod
	call get_drv_paras
	call get_phy_mem
	cmpl $MEM_REQUEST,%eax
	jae 1f
	movw $_err_oom,%ax
	jmp .						#end start,memory size request at least 8M
1:	
	call load_head
	call reset_8253
	call reset_8259a
	smsw %ax
	or $1,%ax
	lmsw %ax
	jmp $8,$0x9000

//{{{check_set_vmod
check_set_vmod:
	push %ds
	mov $0,%ax
	mov %ax,%ds
	mov $0x49,%si
	lodsb
	cmpb $3,%al
	je 1f
	mov $3,%ax
	int $0x10
1:	
	pop %ds
	ret
//}}}
//{{{get_drv_paras		get floppy&harddisk0,1's bios parameters 
get_drv_paras:
	push %ds
	mov $0,%ax
	mov %ax,%ds
	mov $0x78,%bx				#0x1e	floppy's parameters
	mov (%bx),%si
	mov 2(%bx),%ax
	mov %ax,%ds
	mov $12,%cx
	mov $BUFFADDR,%di
	rep movsb
	mov $0,%ax
	mov %ax,%ds
	mov $0x104,%bx				#0x41	hd0 parameters
	mov (%bx),%si
	mov 2(%bx),%ax
	mov %ax,%ds
	mov $16,%cx
	rep movsb
	mov $0,%ax
	mov %ax,%ds
	mov $0x118,%bx				#0x46	hd1 parameters
	mov (%bx),%si
	mov 2(%bx),%ax
	mov %ax,%ds
	mov $16,%cx
	rep movsb
	pop %ds
	ret
//}}}
//{{{get_phy_mem		get physical memory size
get_phy_mem:
	movl $SMAP,%edx
	movl $20,%ecx
	movl $0,%ebx
	movl $0,%esi
1:
	movl $0x400,%edi
	movl $0xe820,%eax
	int $0x15
	jc 2f
	addl 8(%edi),%esi
	cmpl $0,%ebx
	jne 1b
	jmp 3f
2:
	xorl %esi,%esi
3:
	testl $0x8000,%esi
	jz 4f
	addl $0x8000,%esi
4:
	movl $BUFFADDR,%edi
	addl $0x40,%edi
	movl %esi,%eax
	stosl
	ret
//}}}
//{{{load_head		load head code on harddisk
load_head:
	mov $0x80,%dx
	mov $2,%cx
	mov $0x1400,%bx			#head loaded in 0x9000
	mov $0x211,%ax			#read 17 sectors data from harddisk
	int $0x13
	jnc 1f
	movw $_err_rdd,%ax
	jmp .
1:
	ret
//}}}
//{{{reset_8253		set frequence of kernel toys 1/100 second
reset_8253:
	mov $CMD_8253,%al
	outb %al,$CMD_PORT_8253
	jmp .+2
	movw $FRQ_8253,%ax
	outb %al,$DAT_LCK0_8253
	jmp .+2
	movb %ah,%al
	outb %al,$DAT_LCK0_8253
	ret
//}}}
//{{{reset_8259a	reset interrupt controller
reset_8259a:
	cli
	movb $ICW1,%al
	outb %al,$MASTER_A00_PORT			#send icw1
	jmp .+2
	outb %al,$SLAVE_A00_PORT			#send icw1 slave
	jmp .+2
	movb $ICW2_MASTER,%al
	outb %al,$MASTER_A01_PORT			#send icw2
	jmp .+2
	movb $ICW2_SLAVE,%al
	outb %al,$SLAVE_A01_PORT			#send icw2 slave
	jmp .+2
	movb $ICW3_MASTER,%al
	outb %al,$MASTER_A01_PORT			#send icw3
	jmp .+2
	movb $ICW3_SLAVE,%al
	outb %al,$SLAVE_A01_PORT			#send icw3 slave
	jmp .+2
	movb $ICW4,%al
	outb %al,$MASTER_A01_PORT			#send icw4
	jmp .+2
	outb %al,$SLAVE_A01_PORT			#send icw4 slave
	jmp .+2
	movb $OCW1_MASK,%al
	outb %al,$MASTER_A01_PORT			#send ocw1_mask
	jmp .+2
	outb %al,$SLAVE_A01_PORT			#send ocw1_mask slave
	jmp .+2
	ret
//}}}



gdt:	.word	0,0,0,0
		.word	0x7ff,0,0x9a00,0x00c0		#0x8	text	8M
		.word	0x7ff,0,0x9200,0x00c0		#0x10	data	8M
		.word	0xf,0,0x9208,0x00c0			#0x18	stack	64k
		.word	1,0x8000,0x920b,0x00c0		#0x20	disp	8k
		.word	0x7ff,0,0x9200,0x00c0
	
.org 510
.word	0xaa55


	








