/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	toys进入保护模式的head代码，用于在保护模式下继续初始化系统数据结构及安装中断和驱动函数
 *
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifndef _TOYS_MEM_STAB
_TOYS_MEM_STAB	=	0
.endif
_TOYS_TEMP_HEAD	=	9		#redirection need
.include "defconst.s"
.include "deferrno.s"
.include "defmem.s"
.text
	cld
	movl $GDT_DS,%eax
	movw %ax,%ds
	movw %ax,%es
	movw %ax,%fs
	movw %ax,%gs
	movl $KM_BUF,%edi
	movl $0xffff,%eax
	movl %eax,(%edi)
	movl $GDT_SS,%eax
	movl %eax,4(%edi)
	lss (%edi),%esp
	movl $KM_BASE,%edi
	movl $KM_LEN,%ecx
	shrl $2,%ecx			#size of long
	call _sys_clear
	movl $_err_cls,%eax
	jc 9f
	call copy_bios_para
	movl $IDT_BEG,%edi
	movl $BITMAP_BEG,%ecx
	subl %edi,%ecx
	shrl $2,%ecx
	call _sys_clear
	movl $_err_cls,%eax
	jc 9f
#begin create new gdt
	call crt_gdt
	movl $_err_crt,%eax
	cmpl $0,%ecx
	jne 9f
	xorl %eax,%eax
	call move_head
	xorl %eax,%eax
	call crt_ldt
	cmpl $0,%eax
	jne 9f
	movl $KM_BUF,%edi
	movw $127,(%edi)
	movl $GDT_BEG,2(%edi)
	lgdt (%edi)
	jmp $8,$0x300000
9:
	jmp .

//{{{function _sys_clear		#内核缓冲区的清零函数
/*in:		edi:	buffaddress
  			ecx:	count of long;max length=64k
  out:		CF clear if success
 */	
_sys_clear:
	pushl %eax
	cmpl $0x100000,%ecx		#allowed size:	1M bytes
	ja 8f
	cmpl $TEXT_RUN,%edi		#allowed area: 0~3M
	jae 8f
	xorl %eax,%eax
	rep stosl
	clc
	jmp 9f
8:
	stc
	jmp 9f
9:	
	popl %eax
	ret
//}}}
//{{{function copy_bios_para	#copy floppy & harddisk bios parameter's table
copy_bios_para:
	pusha
	movl $0x7e00,%esi
	movl $KM_FLPP,%edi
	movl $12,%ecx
	rep movsb				#floppy
	movl $KM_HDP,%edi
	movl $32,%ecx
	rep movsb				#hard0,1
	movl $0x7e40,%esi
	movl $KM_MEM,%edi
	movl $4,%ecx
	rep movsb				#mem size
	popa
	ret
//}}}
//{{{crt_gdt
crt_gdt:
	pushl %ebp
	movl %esp,%ebp
	movl $GDT_BEG,%edi
	xorl %eax,%eax
	stosl
	stosl
	movl $1,%eax
	movl $1,%ebx
	movl $KM_MEM,%edx
	movl (%edx),%ecx
	shrl $13,%ecx
	movl $TEXT_BASE,%edx
	pushl %ecx
	pushl %edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#0x8	text
	movl $0,%eax
	movl $1,%ebx
	movl -4(%ebp),%ecx
	shll $1,%ecx
	movl -8(%ebp),%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#0x10	data
	movl $0,%eax
	movl $1,%ebx
	movl $STK_LIM,%ecx
	movl $STK_BASE,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#0x18	stack
	movl $0,%eax
	movl $1,%ebx
	movl $DISP_LIM,%ecx
	movl $DISP_BASE,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#0x20	disp
	movl $2,%eax
	movl $0,%ebx
	movl $40,%ecx
	movl $LDT_BEG,%edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#0x28	ldt
	movl $0,%esi
	pushl $STK3_BASE
	pushl $STK2_BASE
	pushl $STK1_BASE
	pushl $STK0_BASE
1:
	movl $0,%eax
	movl $1,%ebx
	movl $STK_LIM,%ecx
#	movl $STK0_BASE,%edx
	popl %edx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#0x30,0x38,0x40,0x48	ss0~3 for task
	incl %esi
	cmpl $4,%esi
	jb 1b
	movl $KM_BUF,%ebx
	movl $TSS_BEG,(%ebx)
	movl $0,%esi
2:
	movl $3,%eax
	movl $KM_BUF,%ebx
	movl (%ebx),%edx
	movl $0,%ebx
	movl $TSS_LIM,%ecx
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl						#0x50,0x58,0x60,0x68	tss0~3
	movl $KM_BUF,%ebx
	movl (%ebx),%eax
	addl $TSS_LIM,%eax
	movl %eax,(%ebx)
	incl %esi
	cmpl $4,%esi
	jb 2b
#	movl -8(%edi),%eax
#	movl -4(%edi),%edx
#	jmp .
	clc
	jmp 9f
8:
	stc
9:	
	movl %ebp,%esp
	popl %ebp
	ret
//}}}
//{{{function move_head		move the final head code to 3M
move_head:
	movl $0x9600,%esi
	movl $TEXT_RUN,%edi
	movl $0x700,%ecx
	rep movsl
	ret
//}}}
//{{{function crt_ldt
crt_ldt:
	pushl %ebp
	movl %esp,%ebp
	movl $LDT_BEG,%edi
	xorl %eax,%eax
	stosl
	stosl						#遗漏了这个？？？？？
	movl $0x101,%eax			#ldt,text
	movl $1,%ebx				#granularity,4k
	movl $KM_MEM,%esi
	movl (%esi),%edx
	shrl $1,%edx
	movl %edx,%ecx
	shrl $12,%ecx
	pushl %edx
	pushl %ecx
	call crt_segdesc			#create 0xf;text	ldt's text seg
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl
	movl $0x100,%eax			#ldt,data
	movl $1,%ebx				#granularity
	popl %ecx
	popl %edx
	shll $1,%ecx				#for test
	movl $0,%edx				#for test
	call crt_segdesc			#create 0x17;data	ldt's data seg
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl
	movl $0x100,%eax
	movl $1,%ebx
	movl $KM_MEM,%esi
	movl (%esi),%edx
	movl $USTK_LIM,%ecx
	shll $12,%ecx
	subl %ecx,%edx
	movl $USTK_LIM,%ecx			#default usr model's stack=16k
	call crt_segdesc			#create 0x1f,stack	ldt's stack seg
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl
	movl $0x100,%eax
	movl $1,%ebx
	movl $2,%ecx
	movl $0xb8000,%edx			#create 0x27,display buffer
	call crt_segdesc
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl
#建立一个任务门，用于测试特权级转移下任务门的调用
	movl $0x104,%eax			#ldt下建立任务门
	movl $0,%ebx
	movl $GDT_TSS1,%ecx			#tss1 段选择符
	movl $0,%edx				#anyone
	call crt_segdesc			#create 0x2f,task gate
	cmpl $0,%ecx
	jne 8f
	stosl
	xchgl %eax,%edx
	stosl
	xorl %eax,%eax
	jmp 9f
8:
	movl $_err_crt,%eax
9:
	movl %ebp,%esp
	popl %ebp
	ret
//}}}





.include "src/kern.s"		#need functios:crt_segdesc

.org 1534
.word	0xbb66

