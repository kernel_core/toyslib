/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	toys的首个任务代码，
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifndef _TOYS_MEM_STAB
_TOYS_MEM_STAB	=	0
.endif
.include "defconst.s"
.include "defmem.s"
.text
	movl $0x17,%eax
	movw %ax,%ds
	movw %ax,%gs
	movw %ax,%fs
	movw %ax,%es
	movl $TSS_BEG,%esi
	movl TSS_LDT(%esi),%eax
	movl TSS_CS(%esi),%ebx
	movl TSS_EIP(%esi),%ecx
	movl TSS_CR3(%esi),%edx
	jmp .



.org 508
.ascii	"toys"
