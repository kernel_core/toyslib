/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	一个通用的启动引导和内核初始化的头文件 
 *
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */

.ifdef	_TOYS_16_
//{{{code16 const
.code16
SMAP			=	0x534d4150
BOOTSEG			=	0x7c0
BOOTADDR		=	0x7c00
DISPSEG			=	0xb800
STKSEG			=	0x2000
STKSP			=	0x3ff
BUFFADDR		=	0x200

#==========8253
FRQ_8253		=	11930
CMD_8253		=	0x36
CMD_PORT_8253	=	0x43
DAT_LCK0_8253	=	0x40
#==========8259a
MASTER_A00_PORT	=	0x20					#主芯片端口 a0=0，用于发送icw1，ocw2
MASTER_A01_PORT	=	0x21					#主芯片端口 a0=1，用于发送icw2，icw3，icw4，ocw1
SLAVE_A00_PORT	=	0xa0					#从芯片端口 a0=0，用于发送icw1，ocw2
SLAVE_A01_PORT	=	0xa1					#从芯片端口 a0=1，用于发送icw2，icw3，icw4，ocw1
ICW1			=	0x11					#icw1命令：边沿触发中断，多片级联，需要icw4
ICW2_MASTER		=	0x20					#icw2命令：设定主芯片的中断处理起始号：0x20
ICW2_SLAVE		=	0x28					#icw2命令：设定从芯片的中断处理起始号：0x28
ICW3_MASTER		=	4						#icw3命令：设定级联端口，主芯片：4
ICW3_SLAVE		=	2						#icw3命令：设定级联端口，从芯片：2
ICW4			=	1						#icw4命令：设定工作模式：普通全嵌套，非缓冲，非自动结束，8086模式
OCW1_MASK		=	0xff					#ocw1命令：中断屏蔽
OCW1_UNMASK		=	0						#ocw1命令：中断非屏蔽
OCW2			=	0x20					#ocw2命令：非自动结束方式，发送EIO通知中断处理完成
//}}}
.else
//{{{code32 const
.code32
#type of segment
G_TEXT			=	0x9a				#GDT 代码段
G_DATA			=	0x92				#GDT 数据段
G_LDT			=	0xe2				#GDT 系统段LDT	特权级3
G_TSS			=	0xe9				#GDT 系统段TSS	特权级3
L_TEXT			=	0xfa				#LDT 代码段
L_DATA			=	0xf2				#LDT 数据段
GATE_INT		=	0x8e00				#GATE 中断门	特权级0
GATE_TRAP		=	0xef00				#GATE 陷阱门	特权级3
GATE_TASK		=	0xe500				#GATE 任务门	特权级3	;0x85 特权级0
GATE_CALL		=	0xec00				#GATE 调用门	特权级3；0x8c 特权级0
GRAN_SEG_4K		=	0xc0				#代码，数据段的颗粒度 4k
GRAN_SEG_1B		=	0x40				#代码，数据段的颗粒度 1byte
GRAN_GT_4K		=	0x80				#门，系统段的限长颗粒度 4k
GRAN_GT_1B		=	0					#门，系统段的限长颗粒度 1byte
# GDT segment selecetor
GDT_CS			=	0x8					#text
GDT_DS			=	0x10				#data
GDT_SS			=	0x18				#stack
GDT_DISP		=	0x20				#disp
GDT_LDT			=	0x28				#ldt
GDT_SS0			=	0x30				#task0 kernel ss
GDT_SS1			=	0x38				#task1 kernel ss
GDT_SS2			=	0x40				#task2 kernel ss
GDT_SS3			=	0x48				#task3 kernel ss
GDT_TSS0		=	0x50				#tss0
GDT_TSS1		=	0x58				#tss1
GDT_TSS2		=	0x60				#tss2
GDT_TSS3		=	0x68				#tss3
# LDT segment selector
LDT_CS			=	0xf					#text
LDT_DS			=	0x17				#data
LDT_SS			=	0x1f				#stack
LDT_DISP		=	0x27				#disp
LDT_TSS1		=	0x2f				#tsak
#buffer length & segment limit defined	moved form --defmem.s--
TEXT_LIM			=	0x400				#4M	 used for calc segdesc, G=1,size=4k;1-base
KM_LEN				=	0x500				#系统数据区长度，终止于0x2f0500
KM_BUF_LEN			=	0xf000				#系统缓冲区长度，60k
GDT_LIM				=	0x8000				#gdt表最大长度，最大4095个	4096*8 1-base for calc segdesc
LDT_LIM				=	0x100				#ldt表最大长度；1-base
PDT_MAX				=	127					#最大的固定位置页目录、页表数
#下面是与运行时无关的定义:
STK_LIM				=	0x10				#stack's limit	1-base;size=64k;G=1;for crt_segdesc
TSS_LIM				=	0x100				#tss's limit;1-base;size=256bytes;G=0;for crt_segdesc
DISP_BASE			=	0xb8000				#display's buffer
DISP_LIM			=	2					#8K;1-base;G=1;size=8k;for crt_segdesc
DMA_BASE			=	0x9d000				#dma base;end of 0x9f400
DMA_LEN				=	0x2400				#dma最大缓冲区长度9k,对应软驱的一个track，18个扇区
USTK_LIM			=	4					#user's stack;1-base;G=1;size=16k;for crt_segdesc
#下面定义tss结构偏移：
TSS_ESP0			=	4
TSS_SS0				=	8
TSS_ESP1			=	0xc
TSS_SS1				=	0x10
TSS_ESP2			=	0x14
TSS_SS2				=	0x18
TSS_CR3				=	0x1c
TSS_EIP				=	0x20
TSS_EFLAGS			=	0X24
TSS_EAX				=	0X28
TSS_ECX				=	0X2C
TSS_EDX				=	0X30
TSS_EBX				=	0X34
TSS_ESP				=	0X38
TSS_EBP				=	0X3C
TSS_ESI				=	0X40
TSS_EDI				=	0X44
TSS_ES				=	0X48
TSS_CS				=	0X4C
TSS_SS				=	0X50
TSS_DS				=	0X54
TSS_FS				=	0X58
TSS_GS				=	0X5C
TSS_LDT				=	0X60
TSS_BITMAP			=	0X64






//}}}
.endif
MEM_REQUEST		=	0x800000		#memory request at least 8M

.ifdef	_TOYS_KERN_
BUFS			=	0x101000		#temp buffer


.endif
.ifdef	_TOYS_TEMP_HEAD
ADDR_RUNN		=	0x9000
.else
ADDR_RUNN		=	0
.endif
