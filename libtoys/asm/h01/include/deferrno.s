/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	一个通用的启动引导和内核初始化的头文件 
 *
 *  Copyright (c) 2023-2025 tybitsfox <tybitsfox@126.com>
 */

//{{{ err in boot
#.ifdef _TOYS_16_
_err_oom		=		0x1234			#memory request at least 8M
_err_rdd		=		0x2345			#load head error
_err_cls		=		0x9a00a9		#_sys_clear error
_err_crt		=		0x9b00b9		#crt_segdesc error

#.endif
//}}}
