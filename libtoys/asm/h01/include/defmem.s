/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	一个通用的启动引导和内核初始化的头文件 
 *	本文件定义内存分配相关的常量
 *	20230417	重新定位了系统表的存储位置，因为在首次进入保护模式时，代码段依旧占据着0～1M内存，不方便对系统表的初始化。
 				现调整系统表所在位置位于1～2M内存位置。并将稳定后的保护模式代码指定到3～4M位置执行。
				目前的内存分配：
				0～1M范围：	DMA缓冲区：DMA_BASE	0X99000~~0X9B400;	LEN:0X2400
							显示缓冲区：DISP_BASE	0XB8000~~0XBA000;	LEN:0X2000
							shadow bios区域，upper 0XBA000；
							内存使用位图：0～0X20000	128K映射4G内存

				对段限长的定义采用1-base进行，便于浏览。即传递给crt_segdesc函数的段限长采用的是1-base格式定义的。

 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifdef	_TOYS_MEM_TEMP
//{{{ 这里定义的是首次进入保护模式时，内存的分配状况.
#Note:这里的定义并没有真正使用，仅仅作为一个参照。在实模式下获取的参数表和内存大小直接拷贝至稳定形态下的位置了
TEXT_BASE			=	0					# offset=0x8000
TEXT_LIM			=	0x7ff				#8M
STK_BASE			=	0x80000				#				top of stack: 0x8ffff
//}}}
.endif
.ifdef	_TOYS_MEM_STAB
//{{{ 这里定义的是进入稳定形态的内核时，内存的分配状况
TEXT_BASE			=	0					#最终代码段起始地址
TEXT_RUN			=	0x300000			#内核代码的开始执行地址
#TEXT_LIM			=	0x400				#4M
STK_BASE			=	0x3f0000			#top of stack: 0x3fffff
STK0_BASE			=	0x3e0000
STK1_BASE			=	0x3d0000
STK2_BASE			=	0x3c0000
STK3_BASE			=	0x3b0000
KM_BASE				=	0x2f0000			#系统数据区：保存软驱参数，硬盘参数，光表位置，物理内存大小，软驱中断标志等
#KM_LEN				=	0x500				#系统数据区长度，终止于0x2f0500
KM_FLPP				=	0x2f0000			#软驱参数的起始地址		12bytes
KM_FINT				=	0x2f000c			#软驱中断标志			4bytes
KM_HDP				=	0x2f0010			#硬盘参数的起始地址		16bytes
KM_COUNTER			=	0x2f0030			#计数器位置				4bytes
KM_MEM				=	0x2f0038			#物理内存大小			4bytes
KM_CURSOR			=	0x2f0040			#光标位置				4bytes
#硬盘驱动所用缓冲：	0x2f0050~~0x2f0070
KM_HDCMDIDX			=	0x2f0050			#硬盘的当前操作命令		4bytes
KM_HDCMDERR			=	0x2f0054			#硬盘命令错误后返回值	4bytes
KM_HDBUFF			=	0x2f0058			#保存硬盘读写缓冲区地址 4bytes
KM_HDLEFT			=	0x2f005c			#剩余的扇区读写次数		1byte
KM_HDPREC			=	0x2f0060			#写补偿					1byte
KM_HDSECC			=	0x2f0061			#读/写的扇区数			1byte
KM_HDSECB			=	0x2f0062			#扇区起始号(1-base)		1byte
KM_HDCYLL			=	0x2f0063			#16位柱面低8位			1byte
KM_HDCYLH			=	0x2f0064			#柱面号高8位			1byte
KM_HDDRVHD			=	0x2f0065			#0b101驱动器号/磁头号	1byte
KM_HDRT				=	0x2f0066			#状态寄存器的返回值		1byte
KM_HDERR			=	0x2f0068			#自定义的硬盘错误代码	4bytes
#软驱的返回参数及错误缓冲：0x2f0070~0x2f00f0
KM_FLPRT			=	0x2f0070			#FDC返回参数缓冲区 base:0x2f0070;size:0x50;
KM_FLPERR			=	0x2f00c0			#FDC错误代码存放;  base:0x2f00c0；size=0x20;
KM_UPD				=	0x2f0100			#用户页目录表地址缓冲	4bytes; max:64
#系统缓冲区 60k
KM_BUF				=	0x2f1000			#系统缓冲区起始地址,0x2f1000~~0x2fffff;60k
#KM_BUF_LEN			=	0xf000				#系统缓冲区长度，60k
#下面的位置暂时没有确定
IDT_BEG				=	0x100000			#idt 位置
GDT_BEG				=	0x100900			#gdt 位置
#GDT_LIM			=	0x8000				#gdt表最大长度，最大4095个；1-base for crt_segdesc
LDT_BEG				=	0x108a00			#首个ldt 位置，每个ldt最大32个描述符;预留6个ldt;结束地址：0x109000
#LDT_LIM			=	0x100				#ldt表最大长度
SYS_LINK			=	0x109000			#tss,pdt,pt表链信息，size=4096*9=0x9000
TSS_BEG				=	0x112000			#tss链中首个地址块，存放112个*256字节的tss;	end:0x119000
PDT_BEG				=	0x119000			#pdt链中首个页目录表地址块，存放128个页目录表;	end:0x199000
#PDT_MAX			=	127					#最大的固定位置页目录、页表数
BITMAP_BEG			=	0x199000			#由此地址开始位图标识每个4k空间的使用状况。之前的内存固定使用

#STK0				=	0X9C000				#ring0级堆栈，稳定态内核最先使用的堆栈，size=16k; top=0x9ffff
//}}}
.endif
#下面是与运行时无关的定义:
#STK_LIM				=	0x10				#stack's limit
#DISP_BASE			=	0xb8000				#display's buffer
#DISP_LIM			=	2					#8K
#DMA_BASE			=	0x9d000				#dma base;end of 0x9f400
#DMA_LEN				=	0x2400				#dma最大缓冲区长度9k,对应软驱的一个track，18个扇区
#USTK_LIM			=	4					#user's stack size=16k
