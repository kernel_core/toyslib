/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	toys进入稳定保护模式的head代码，本文件主要实现head下对内存的统计，管理，分配和回收。
 *	1、使用位图对整个内存区域包括ldt映射的用户区域的内存的使用情况统计。
 *	2、使用页表的A字段协助对内存的使用情况进行统计和标识。
 *	3、位图采用3级索引：
 *	   1级为标识内核区域或用户区域，
 *	   2级为在1级范围内按4M区域标识，
 *	   3级为位图索引区域4K。
 *	   位图按bit位标识索引区域的使用情况，每一bit位代表一个4k页面，每字节可索引32k。	   
 *	4、每页位图可索引128M内存区域，位图存放物理地址固定为：0
 *  Copyright (c) 2023-2025 tybitsfox <tybitsfox@126.com>
 */
//{{{ function mbm_clear_range	memory-bit-map	位图空间清零函数
mbm_clear_range:
	movl $0,%edi
	movl $0,%eax
	movl $0xa000,%ecx
	rep stosl				#clear 0~~0x27fff位图区域
	ret
//}}}
//{{{ function mbm_check_bit	测试指定地址位图
/*in:		eax:	address
  out:		eax:	=0 free;	=1 used
 */
mbm_check_bit:
	pushl %ebx
	pushl %ecx
	shrl $12,%eax
	movl %eax,%ecx
	movl %eax,%ebx
	shrl $3,%ebx
	movb (%ebx),%al
	andl $7,%ecx
	shrb %cl,%al
	andl $1,%eax
	popl %ecx
	popl %ebx
	ret
//}}}	
//{{{ function mbm_set_bit	设置指定地址的位图
/*in:		eax:	address
  			ebx:	=1 set uesd; =0 set free
 */
mbm_set_bit:
	pushl %ecx
	pushl %esi
	andl $1,%ebx
	shrl $12,%eax
	movl %eax,%ecx
	shrl $3,%eax
	movl %eax,%esi
	movb (%eax),%bh
	andl $7,%ecx
	movl $1,%eax
	shlb %cl,%al
	cmpb $1,%bl
	jne  1f				#set free
	orb %al,%bh
	jmp 2f
1:
	notb %al
	andb %al,%bh
2:
	movb %bh,(%esi)
	popl %esi
	popl %ecx
	ret
//}}}
//{{{ function mbm_check_all	全位图区域的测试函数，并生成空闲地址链接
/*in:		none
  out:		eax：	返回的空闲位图链表地址
  返回链表值结构：
  高17位：			为32k对齐的地址索引
  位12(0-base)：	若置位则表示存在连续多个32k的空闲地址，空闲个数保存在低14位，最大连续4M，0x80
  					若复位则表示不连续，在本32k区域内存在若干4k个空闲地址。
  链表结束标志：0x0
  链表地址保存至0x21000,最大0x7000
*/
mbm_check_all:
	pushl %ebp
	movl %esp,%ebp
	movl $0,%esi
	movl $0x21000,%edi
	movl $KM_MEM,%ecx
	shrl $15,%ecx			#根据物理内存地址，确定位图长度
#	movl $0x20000,%ecx
	movl $0,%edx
1:
	lodsb
	cmpb $0,%al
	ja 3f
	incl %edx
	cmpl $0x80,%edx			#4M
	jb 2f
	movl %esi,%ebx
#	decl %ebx
	subl %edx,%ebx
	shll $15,%ebx
	addl %edx,%ebx
	orl $0x1000,%ebx
	movl %ebx,%eax
	stosl
	movl $0,%edx
2:
	loop 1b
	jmp 9f	
3:	#连续被中断了,需保存之前连续的区域
	cmpl $0,%edx
	je 4f
	pushl %eax
	movl %esi,%ebx
	subl $2,%ebx	#跳过当前字节位置
	subl %edx,%ebx
	shll $15,%ebx
	addl %edx,%ebx
	orl $0x1000,%ebx
	movl %ebx,%eax
	stosl
	movl $0,%edx
	popl %eax
4:
	cmpb $0xff,%al
	jne 5f
	loop 1b
	jmp 9f
5:
	movl %esi,%ebx
	decl %ebx
	shll $15,%ebx
	andl $0xff,%eax
	addl %ebx,%eax
	stosl
	loop 1b
9:
	movl $0,%eax
	stosl
	movl $0x21000,%eax
	movl %ebp,%esp
	popl %ebp
	ret
//}}}

//{{{ function mbm_set_range	设置区间位图
/*in:	eax:	区间开始地址（包含该地址所在页面）
  		ebx:	区间结束地址（包含该地址所在页面）
  out:	none
*/
mbm_set_range:
	pushl %ebp
	movl %esp,%ebp
	cmpl %eax,%ebx
	jae 1f
	xchgl %eax,%ebx
1:	
	pushl %eax
	pushl %ebx
	movl -4(%ebp),%eax
	movl -8(%ebp),%ebx
	subl %eax,%ebx
	cmpl $0x8000,%ebx		#if more than 1byte
	jae 3f
2:
	movl -4(%ebp),%eax
	movl $1,%ebx
	call mbm_set_bit
	movl -4(%ebp),%eax
	addl $0x1000,%eax
	cmpl -8(%ebp),%eax
	ja 9f
	movl %eax,-4(%ebp)
	jmp 2b
3:
	movl -4(%ebp),%edi
	shrl $12,%edi
	movl %edi,%ecx
	andl $7,%ecx
	movl $0xff,%eax
	shrl $3,%edi
	movb (%edi),%bl
	shlb %cl,%al
	orb %bl,%al
	stosb					#first byte finished
4:	
	movl %edi,%esi
	shll $15,%esi
	addl $0x8000,%esi
	cmpl -8(%ebp),%esi
	ja 5f					#middle bytes finished
	movb $0xff,%al
	stosb
	jmp 4b
5:
	movl -8(%ebp),%edi
	shrl $12,%edi
	movl %edi,%eax
	andl $7,%eax
	movl $7,%ecx
	subl %eax,%ecx
	movl $0xff,%eax
	shrl $3,%edi
	movb (%edi),%bl
	shrb %cl,%al
#	jmp .
	orb %bl,%al
	stosb					#last byte finished
9:
	movl %ebp,%esp
	popl %ebp
	ret
//}}}
//{{{ function init_mbitmap		内存使用位图的初始化函数
/*本函数根据内核加载后使用的地址以及初始化确定使用的固定内存区域，列出内存位图，在用=1；可用=0；
  内存使用确定的依据为：
  1、内核代码加载位置以及长度；
  2、defmem.s中列出的明确使用的固定位置；
  3、在执行本函数时，尚未进入用户模式，所有分配给用户模式的内存皆为可用
  4、物理内存0~~0x27fff内存位图所占区域，
  6、0xA0000~0xfffff 包含显示缓冲区和bios映射区域和其他rom bios区域
*/
init_mbitmap:
	pushl %ebp
	movl %esp,%ebp
	call mbm_clear_range
	movl $TEXT_RUN,%eax
	movl $8191,%ebx
	addl %eax,%ebx
	call mbm_set_range			#设置head初始化代码区域
	movl $0,%eax
	movl $0x27fff,%ebx
	call mbm_set_range			#设置位图区域
	movl $STK3_BASE,%eax
	movl $STK_BASE,%ebx
	movl $STK_LIM,%ecx
	shll $12,%ecx				#get stack's length
	decl %ecx
	addl %ecx,%ebx
	call mbm_set_range			#设置4个内核堆栈段的区域
	movl $KM_FLPP,%eax
	movl $TEXT_RUN,%ebx
	decl %ebx
	call mbm_set_range			#设置系统参数区和缓冲区的区域
	movl $IDT_BEG,%eax
	movl $BITMAP_BEG,%ebx
	decl %ebx
	call mbm_set_range			#设置系统表占用的区域
	movl $0xa0000,%eax
	movl $0xfffff,%ebx
	call mbm_set_range			#设置bios映射区及其他roms及显存区域
	movl %ebp,%esp
	popl %ebp
	ret
//}}}



