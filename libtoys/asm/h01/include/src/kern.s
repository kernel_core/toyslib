/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 *	toys进入保护模式的head后，以汇编实现的内核功能函数，包括段描述符的建立函数...
 *
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifdef	_TOYS_MEM_STAB

//{{{ function crt_segdesc	建立一个段描述符
/*本函数可建立gdt/ldt所用的代码段、数据段描述符，以及系统段和门描述符
1、本函数只生成规定特权级的描述符，不会在GDT生成特权级为3的数据和代码段，不会在LDT中生成特权级0的代码和数据段；
对门的描述符同样有限制：不生成特权级0的陷阱门，特权级3的中断门以及特权级0的任务门。
本函数忽略调用门的生成。
2、本函数具有一定的错误检测能力：
（1）、对生成不同特权级的描述符的限制检测，
（2）、对参数的格式，描述符类型有效索引：0-6
（3）、对段界限引用的大小(最大不得超过物理内存,最小不能小于0xf字节)是否超出都可进行简单的检测
（4）、对基地址检测同段界限检测。
Note:输入参数：
eax:	al:段类型；=0 数据段；=1 代码段；=2 ldt；=3 tss；=4 任务门；=5 中断门；=6 陷阱门；
		--0x9a, 0x92, 0xfa, 0xf2 ,0xe2, 0xe9, 0xe5, 0x8e, 0xef
		ah: =0 gdt；=1 ldt；
ebx:	bl:颗粒度；=0 byte；=1 4k。		 --0xc 0x4
ecx:	段限长(对于门是段选择符)； 低20位有效，还要结合颗粒度判断长度不超过实际内存大小
edx:	段基地址（对于门是入口地址）；
默认设置： 存在位=1；描述符类型：=1 (数据或代码段) =0 (系统段或门)	D/B位： =1 (32位代码段或数据段) =0 (系统段或门)
输出：
if  ecx == 0
			eax:edx 保存构造好的8字节段描述符，eax保存底4字节，edx保存高四字节.
else
 	        ecx == errorcode

为减少代码量使用下列变量减少分支判断：
stype0:	.byte	0x92,0x9a,0xe2,0xe9,0xe5,0x8e,0xef		#定义段类型  调用门0xec忽略
stype3:	.byte	0xf2,0xfa,0,0,0xe5,0,0
slim0:	.byte	0x40,0x40,0,0,0,0,0
slim1:	.byte	0xc0,0xc0,0x80,0,0,0,0					#ldt允许设置和使用颗粒度；
*/	
crt_segdesc:
	pushl %ebp
	movl %esp,%ebp
	pushl %eax
	pushl %ebx
	pushl %ecx
	pushl %edx
	pushl %edi
	pushl %esi
#先对所有的参数有效性进行检测，然后再组合成描述符 1、无须检测，自动生成。
#2、参数格式：描述符索引，gdt/ldt类型，颗粒度，段限长，段基地址，内存范围检测
	movl -4(%ebp),%eax
	cmpb $7,%al				#描述符格式
	jb 1f
	movl $0x1fff,%ecx
	jmp 9f
1:
	cmpb $2,%ah
	jb 2f					#表类型
	movl $0x1ffe,%ecx
	jmp 9f
2:
	movl -8(%ebp),%eax
	cmpb $2,%al				#颗粒度
	jb 3f
	movl $0x1eff,%ecx
	jmp 9f
3:
	movl -12(%ebp),%eax
	cmpl $0x100000,%eax		#段限长
	jb 4f
	movl $0x2fff,%ecx
	jmp 9f
4:
	cmpl $0xf,%eax			#下限，不得小于15字节
	jae 5f 
	movl -8(%ebp),%ebx
	cmpb $0,%bl				#颗粒度=1时允许
	jne 5f
	movl -4(%ebp),%ebx
	cmpb $3,%bl
	ja 5f
	movl $0x2ffe,%ecx
	jmp 9f
5:
	movl -16(%ebp),%eax
	movl $KM_MEM,%ebx
	cmpl (%ebx),%eax
	jb 1f
	movl $0x3fff,%ecx
	movl (%ebx),%esi
	jmp 9f
#单项输入检测完成，开始内存范围检测，基地址+限长<物理内存,（限门以外的描述符）
1:
	movl -4(%ebp),%eax
	cmpb $3,%al
	ja 1f
	movl -8(%ebp),%eax
	movl -12(%ebp),%edx
	cmpb $0,%al
	je 2f
	shll $12,%edx
2:
	clc
	movl -16(%ebp),%eax
	adcl %edx,%eax
	jnc 3f
	movl $0x4fff,%ecx
	jmp 9f
3:
	movl $KM_MEM,%ebx
	cmpl (%ebx),%eax
	jbe 1f
	movl $0x4ffe,%ecx
	jmp 9f
#范围检测完成，开始组合检测 ：如果是ldt,tss,getes的话：ldt选项错误，颗粒度无效。
#Note:但！任务门和ldt组合有效。
1:	
	movl -4(%ebp),%eax
	movl -8(%ebp),%ebx
	cmpb $0,%ah
	je 2f
	cmpb $2,%al
	jb 2f
	cmpb $4,%al
	je 2f
	movl $0x5fff,%ecx
	jmp 9f
2:							#wrong check over
	movl -4(%ebp),%eax
	cmpb $0,%ah
	jne 1f
	leal stype0,%esi
	jmp 2f
1:
	leal stype3,%esi
2:
	addl $ADDR_RUNN,%esi
	andl $0x7,%eax
	addl %eax,%esi
	lodsb
	movl %eax,%edx			#save in edx
	movl -8(%ebp),%ebx		#load ebx = granularity
	movl -4(%ebp),%eax
	cmpb $0,%bl
	jne 3f
	leal slim0,%esi
	jmp 4f
3:
	leal slim1,%esi
4:
	addl $ADDR_RUNN,%esi
	andl $7,%eax
	addl %eax,%esi
	lodsb
	movb %dl,%ah
	movw %ax,%dx			#now dl=G/DB/0AVL/limit/,dh=P/DPL/S/TYPE
	movl -12(%ebp),%eax
	movl -4(%ebp),%ebx
	cmpb $3,%bl
	ja 5f
	decl %eax
5:	
	rorl $16,%eax
	addl %eax,%edx			# limit in dl is available;low-16==>high16;edx is finished
	movl -16(%ebp),%eax
	xchgw %ax,%dx			#gate
	xchgl %eax,%edx			#gate finished
	movl -4(%ebp),%ebx
	cmpb $4,%bl
	jb 1f
	xorl %ecx,%ecx
	jmp 9f
1:
	bswap %edx
	rorl $8,%edx
	xorl %ecx,%ecx
	rorl $16,%eax
9:
	popl %esi
	popl %edi
	movl %ebp,%esp
	popl %ebp
	ret
//}}}
/*由于包含本文件的boot/head.s在编译时并未指定所需的重定位信息（同一目录下还生成别的链接模块）。因此下列标识符的位置会产生重定位错误，无法正确访问，因此需要手动调整，加上重定位信息：ADDR_RUNN*/
stype0:	.byte	0x92,0x9a,0xe2,0xe9,0xe5,0x8e,0xef		#定义段类型  调用门0xec忽略
stype3:	.byte	0xf2,0xfa,0,0,0xe5,0,0
slim0:	.byte	0x40,0x40,0,0,0,0,0
slim1:	.byte	0xc0,0xc0,0x80,0,0,0,0					#ldt允许设置和使用颗粒度；


.endif




