/* SPDX-License-Identifier: GPL-2.0+
 *  --toys-- 
 * 	AT硬盘的驱动程序文件
 *	测试用镜像硬盘的参数：CHS=40*8*63=20160*512=10M
 *	--20230413对函数参数的传递方式进行调整，应为涉及中断函数调用，
 *	--同时硬盘所有命令参数具有一致性的特点，将参数放置在指定缓冲区中提供函数使用。	
 *	--20230424写硬盘测试成功，但在预期之外：即写硬盘操作产生的中断是在执行了写入操作之后，而非
 *	向硬盘驱动器发送完命令之后，之前硬盘写操作一直测试不成功就是这个原因，同样还应有格式化命令
 *	应该也是这种情况。
 *  Copyright (c) 2023-2024 tybitsfox <tybitsfox@126.com>
 */
.ifndef	_TOYS_HDD_
_TOYS_HDD_	=	0
.endif
.include "defports.s"

//{{{hd_controller_reset	硬盘控制器的复位操作，当硬盘是非引导盘时，必须要先复位控制器，再执行参数建立函数才可正常操作
/*in:		none
  out:		CF clear if success
*/
hd_controller_reset:
#复位操作将写补偿字节取出
	movl $KM_HDP,%ebx
	movb 5(%ebx),%al
	shrb $2,%al
	movl $KM_HDPREC,%ebx
	movb %al,(%ebx)
	movl $4,%eax
	movl $HDP_CMD,%edx
	outb %al,%dx
	movl $10,%eax
	int $0x80
	call hd_check_status
	ret
//}}}
//{{{hd_check_status	硬盘操作的初始化函数：硬盘的控制器检测，命令发送，驱动器状态检测
/*in:		none 硬盘控制器命令字	0xc8 or 0x8 HD_CONTROL_BYTE
  out:		CF clear if success
*/
hd_check_status:
	movl $KM_HDERR,%ebx
	movl $20,%ecx
1:	
	movl $HDP_STAT,%edx
	inb %dx,%al
	testb $HDST_BUSY,%al
	jz 2f
	movl $4,%eax
	int $0x80
	loop 1b
	movl $0x1a0,(%ebx)
	stc
	jmp 9f
2:
	movl $HD_CONTROL_BYTE,%eax
	movl $HDP_CMD,%edx
	outb %al,%dx					#send control command : more than 8 heads;disable ECC;disable retry
	jmp .+2
	jmp .+2		
	movl $40,%ecx
3:
	movl $HDP_STAT,%edx
	inb %dx,%al
	jmp .+2
	testb $HDST_READY,%al
	jnz 4f
	movl $4,%eax
	int $0x80
	loop 3b
	movl $0x1a1,(%ebx)
	stc
	jmp 9f
4:
	clc
9:
	ret
//}}}
//{{{hd_get_paras	取得硬盘基本参数表
/*in:	al: index
  out:	al: para by index
 */	
hd_get_paras:
	pushl %esi
	movl $KM_HDP,%esi
	andl $0xf,%eax
	addl %eax,%esi
	lodsb
	popl %esi
	ret
//}}}
//{{{hd_ports_set	硬盘控制器的参数设置-----硬盘操作之前的参数设置函数
/*in:		：		0x1f1	写补偿，取自参数表，无需传入
  			al:		扇区数	0x1f2
			ah:		起始扇区号	0x1f3
			bl:		低字节cylinder号	0x1f4
			bh:		高字节cylinder号	0x1f5
			cl:		磁头号
			ch:		驱动器号；cx需要组合为端口0x1f6的参数 0b101dhhhh
			dl:		命令码，
  out:		CF clear if success
*/
hd_ports_set:
#frist,test KM_HDCMDIDX = 0? false return;true go on
	pushl %ebp
	movl %esp,%ebp
	pushl %eax
	pushl %ebx
	pushl %ecx
	pushl %edx
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $KM_HDCMDIDX,%ebx
	cmpl $0,(%ebx)					#参数设置之前，必须检查命令字是否为0,否则表示前次命令没有执行完
	je 1f
	movl $KM_HDERR,%ebx
	movl $0x1a2,(%ebx)
	stc
	jmp 9f
1:
	movl -16(%ebp),%eax
	cmpb $HDEXE_REST,%al
	jb 8f
	cmpb $HDEXE_SPEC,%al
	ja 8f
	andl $0xff,%eax
	movl %eax,(%ebx)					#save command
	movl $KM_HDPREC,%ebx
	movl $KM_HDP,%edx
	movw 5(%edx),%ax
	shrw $2,%ax
	movb %al,(%ebx)						#save precomp
	movl -4(%ebp),%eax
	movb 14(%edx),%cl
	addb %ah,%al
	jc 8f
	decb %al							#1-base;need dec 1 times
	cmpb %cl,%al						#起始扇区+扇区数>默认的扇区数
	ja 8f
	movl -4(%ebp),%eax
	cmpb $0,%al
	jbe 8f
	movb %al,1(%ebx)					#save sector count
	cmpb $0,%ah
	jbe 8f
	movb %ah,2(%ebx)					#save sector index number
	movl -8(%ebp),%eax
	movb %al,3(%ebx)					#save low  8 bits of cylinder
	movb %ah,4(%ebx)					#save high 8 bits of cylinder
	movl -12(%ebp),%eax
	andb $0xf,%al
	movb 2(%edx),%cl					#bios head count
	cmpb %cl,%al
	jae 8f
	andb $1,%ah
	rolb $4,%ah
	addb $0xa0,%ah
	addb %ah,%al						#format: 0b101dhhhh
	movb %al,5(%ebx)					#save driver & heads
	clc
	movl $KM_HDSECC,%ebx
	movl $KM_HDLEFT,%edx
	movb (%ebx),%al
	movb %al,(%edx)						#save sector counts
	jmp 9f
8:
	movl $KM_HDERR,%ebx
	movl $0x1a3,(%ebx)
	stc
9:
	pop %ds
	movl %ebp,%esp
	popl %ebp
	ret	
//}}}
//{{{hd_cmd_launch	 执行设定的硬盘操作命令
/*in:		none
  out:		CF clear if success
 */	
hd_cmd_launch:
	call hd_check_status
	jc 9f
	push %ds
	movl $GDT_DS,%eax
	movw %ax,%ds
	movl $KM_HDPREC,%esi
	movl $HDP_PRECOMP,%edx			#port 0x1f1
	lodsb							#finished shrb $2
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_SECTC,%edx			#port 0x1f2
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_SECTB,%edx			#port 0x1f3
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_CYLL,%edx				#port 0x1f4
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_CYLH,%edx				#port 0x1f5
	outb %al,%dx
	jmp .+2
	lodsb
	movl $HDP_DRVHD,%edx			#port 0x1f6
	outb %al,%dx
	jmp .+2
	movl $KM_HDCMDIDX,%esi
	lodsb
	movl $HDP_EXE,%edx				#port 0x1f7
	outb %al,%dx
	pop %ds
9:	
	ret
//}}}
//{{{hd_normal_int
hd_normal_int:
	movl $HDP_STAT,%edx
	inb %dx,%al
	testb $HDST_ERR,%al
	jz 1f
	movl $HDP_ERR,%edx
	inb %dx,%al
	movl $KM_HDRT,%ebx
	movb %al,(%ebx)
	stc
	jmp 9f
1:
	movl $100,%ecx
2:	
	movl $HDP_STAT,%edx
	inb %dx,%al
	testb $HDST_READY,%al
	jnz 3f
	jmp .+2
	jmp .+2
	loop 2b
	movl $KM_HDRT,%ebx
	movb $0xff,(%ebx)
	stc
	jmp 9f
3:
	clc
	xorl %eax,%eax
9:	
	ret
//}}}
//{{{hd_read_int	读硬盘操作
hd_read_int:
	movl $KM_HDLEFT,%ebx
	movb (%ebx),%al
	cmpb $0,%al
	ja 1f
	movl $KM_HDCMDIDX,%edx
	movl $0,(%edx)
	jmp 9f
1:
	decb %al
	movb %al,(%ebx)
	movl $256,%ecx
	movl $HDP_DATA,%edx
	movl $KM_HDBUFF,%ebx
	movl (%ebx),%edi
	rep insw
	movl %edi,(%ebx)
9:	
	ret
//}}}	
//{{{hd_write_int	写硬盘、格式化操作
hd_write_int:
	cli
	movl $KM_HDLEFT,%ebx
	movb (%ebx),%al
	cmpb $0,%al
	ja 1f
	movl $KM_HDCMDIDX,%edx
	movl $0,(%edx)
	jmp 9f
1:
	decb %al
	movb %al,(%ebx)
	movl $256,%ecx
	movl $HDP_DATA,%edx
	movl $KM_HDBUFF,%ebx
	movl (%ebx),%esi
	rep outsw
	movl %esi,(%ebx)
9:	
	sti
	ret
//}}}	

//{{{ function do_hd_request	外部接口，可由c调用的硬盘操作函数
/*硬盘驱动器的命令执行函数,本函数设计位符合x86下C语言函数调用规范
 in:		para1:起始扇区号（1-base）			ebp+8
 			para2:扇区数						ebp+12
			para3:cylinder号(0-base)			ebp+16
			para4:磁头号（0-base）				ebp+20
			para5:驱动器号(0-base)				ebp+24
			para6:缓冲区地址					ebp+28
			para7:磁盘操作命令					ebp+32
 out:		return=0 success; else =errno;
 */
do_hd_request:
	pushl %ebp
	movl %esp,%ebp
	movl $KM_HDERR,%ebx
	movl $0,(%ebx)					#clear errno
	movl $KM_HDBUFF,%ebx
	movl 28(%ebp),%eax
	movl %eax,(%ebx)				#save tran's buffer address
	xorl %eax,%eax
	movl 8(%ebp),%eax
	andl $0xff,%eax
	movl 12(%ebp),%ebx
	andl $0xff,%ebx
	shll $8,%ebx
	addl %ebx,%eax					#eax set
	movl 20(%ebp),%ebx
	andl $0xff,%ebx
	movl 24(%ebp),%ecx
	andl $0xf,%ecx
	shll $8,%ecx
	addl %ebx,%ecx					#ecx set
	movl 16(%ebp),%ebx				#ebx set
	movl 32(%ebp),%edx				#edx set
	call hd_ports_set
	jc 9f
	call hd_cmd_launch				#测试发现，写硬盘，格式化操作时，中断信号产生于outsw之后，所以首次调用在中断之外
	jc 9f
	movl $KM_HDCMDIDX,%ebx
	cmpl $HDEXE_WRITE,(%ebx)		#read,write,format
	je 1f
	cmpl $HDEXE_FORMAT,(%ebx)
	je 1f
	jmp 9f
1:
	call hd_write_int
9:
	movl $KM_HDERR,%ebx
	movl (%ebx),%eax
	movl %ebp,%esp
	popl %ebp
	ret
//}}}	














