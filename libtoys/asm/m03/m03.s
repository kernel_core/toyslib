.code16
BOOTSEG		=	0x7c0
BOOTADDR	=	0x7c00
DISPSEG		=	0xb800
VMOD		=	3
SMAP		=	0x534d4150
.text
	jmp $BOOTSEG,$go
go:
	mov %cs,%ax
	mov %ax,%ds
	mov %ax,%es
	lss stk,%sp
	call load_sector
	call clsscr
	xor %ax,%ax
	call set_cur_pos
	call get_phymem
	call show_msg
	cli
	lgdt l_gdt
	smsw %ax
	or $1,%ax
	lmsw %ax 
	jmp $8,$0



#{{{ function load_sector
load_sector:
	clc
	mov $0,%dx
	mov $2,%cx
	mov $0x200,%bx
	mov $0x204,%ax
	int $0x13
	jc 1f
	xor %ax,%ax
	jmp 2f
1:
	mov $0x1234,%ax
	jmp .
2:	
	ret
#}}}	
#{{{ function clsscr
clsscr:
	push %es
	mov $DISPSEG,%ax
	mov %ax,%es
	mov $0x720,%ax
	mov $0x2000,%cx
	mov $0,%di
	rep stosw
	pop %es
	ret
#}}}	
#{{{ function set_cur_pos
set_cur_pos:
	add cursor,%ax
	mov %ax,%bx
	mov $0xf,%al
	mov $0x3d4,%dx
	outb %al,%dx
	jmp .+2
	jmp .+2
	inc %dx
	mov %bl,%al
	outb %al,%dx
	jmp .+2
	jmp .+2
	dec %dx
	mov $0xe,%al
	outb %al,%dx
	jmp .+2
	jmp .+2
	inc %dx
	mov %bh,%al
	outb %al,%dx
	ret
#}}}	
#{{{ function get_phymem
get_phymem:
	movl $0,%ebx
	movl %ebx,%esi
	movl $20,%ecx
	movl $SMAP,%edx
1:
	movl $0x800,%edi
	movl $0xe820,%eax
	int $0x15
	jc 2f
	movl 8(%edi),%eax
	addl %eax,%esi
	cmpl $0,%ebx
	jne 1b
	jmp 3f
2:
	movl $0,%esi
3:
	testl $0x8000,%esi
	jz 4f
	addl $0x8000,%esi
4:
	movl %esi,msize
	ret
#}}}	
#{{{ function get_disp_pos
get_disp_pos:
	mov cursor,%ax
	shl $1,%ax
	ret
#}}}


stk:	.word	0x3ff,0x8000
cursor:	.word	160
msize:	.long	0,0

.org	510
.word	0xaa55

#{{{ function show_msg
show_msg:
	push %es
	call get_disp_pos	#pos return at eax
	mov $DISPSEG,%bx
	mov %bx,%es
	mov %ax,%di			#disp position
	mov $msg,%si
	mov $0xc,%ah
	mov $len,%cx
1:
	lodsb
	stosw
	loop 1b
	mov $len,%ax
	call set_cur_pos
	pop %es
	ret
#}}}
msg:	.ascii "hello OS world!"
len=.-msg
.align	2
l_gdt:	.word 47
		.long BOOTADDR+gdt
.align	8		
gdt:	.word	0,0,0,0
		.word	1,0x8000,0x9a00,0x00c0		#0x8	text	4k
		.word	1,0x8000,0x9200,0x00c0		#0x10	data	4k
		.word	1,0xa000,0x9200,0x00c0		#0x18	stack	4k
		.word	2,0x8000,0x920b,0x00c0		#0x20	disp	8k
		.word	0x3ff,0,0x9210,0x00c0		#0x28	data	4m


.org	1020
.ascii "tian"

