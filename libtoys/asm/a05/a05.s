.bss
buf:	.space 8192,0
.data
st1:	.ascii "test _snprintf %s ...%s...value is: %d...hex is: 0X%X...0x%x...uint:%u...char is:%c...ptr is:%p\n"
.space	10,0
s1:		.ascii "hello world!"
.space	6,0
s2:		.ascii "test snprintf"
.space	2,0
xlen:	.int 0,0
.text
.globl	_start
_start:
	pushl %ebp
	movl %esp,%ebp
	movl $s1,%eax
	pushl %eax
	movl $'c,%eax
	pushl %eax
	movl $-192,%eax
	pushl %eax
	movl $205,%eax
	pushl %eax
	pushl %eax
	pushl %eax
	leal s2,%esi
	pushl %esi
	leal s1,%esi
	pushl %esi
	leal st1,%esi
	pushl %esi
	movl $8192,%eax
	pushl %eax
	movl $buf,%esi
	pushl %esi
	call _snprintf
	movl %ebp,%esp
	popl %ebp 
	movl $0,%ecx
	movl $buf,%esi
1:
	incl %ecx
	lodsb
	cmpb $0,%al
	jne 1b
	decl %ecx
	movl %ecx,%edx
	leal buf,%ecx
	movl $1,%ebx
	movl $4,%eax
	int $0x80
	movl $0,%ebx
	movl $1,%eax
	int $0x80
	ret

