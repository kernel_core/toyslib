# 32 in 64
#关于汇编程序调用汇编函数的约定：
#一、尽可能采用寄存器传递参数；
#二、关于寄存器的保护和传递
#方法一：
#为保持代码精简，调用者和被调用函数寄存器的保护，不采用intel的惯例：调用者保存a\c\d，被调用者保存d/si/di，而是调用者必须保存不得修改的寄存器，即执行状态保存的责任全部在调用者
#方法二：
#采用pusha指令一次性保存所有寄存器，待操作完成后，将需要保存或传递的寄存器，通过堆栈调整然后使用popa出栈所有寄存器。这种方法简单一致，但出入栈消耗大，对复杂的调用这种方法为优选
#寄存器出入栈顺序：EAX，ECX，EDX，EBX，ESP，EBP，ESI，EDI
#假设在没有其他出入栈操作的情况下，如果修改eax，则：movl %eax,28(%esp)；最后popal出栈可获得更新的eax

#			Author:	tybitsfox	20230309	<for toys>

#snprintf prepare only for kernel
/*int sprintf( char *str,const char * format,.........);
％d 整数的参数会被转成一有符号的十进制数字
％u 整数的参数会被转成一无符号的十进制数字
％o 整数的参数会被转成一无符号的八进制数字
％x 整数的参数会被转成一无符号的十六进制数字，并以小写abcdef表示
％X 整数的参数会被转成一无符号的十六进制数字，并以大写ABCDEF表示浮点型数

％c 整型数的参数会被转成unsigned char型打印出。
％s 指向字符串的参数会被逐字输出，直到出现NULL字符为止
％p 如果是参数是“void *”型指针则使用十六进制格式显示。
 */
KA_SNPRINT	=	1
.include "ka_lib.h"
.bss
buf:	.space 4096,0
val:	.int 0
.data
err01:	.string	"Error:	length is too long,out of range!\n"
len01=.-err01
err02:	.string "Error:	syntax error!\n"
len02=.-err02
err03:	.string "Error:	out of memory!\n"
len03=.-err03
.text
.globl	_snprintf
.type _snprintf,@function
#{{{zero_buffer	clear buf and set bits is 0
zero_buffer:
	pushal
	leal buf,%edi
	movl $0,%eax
	movl $1024,%ecx
	rep stosl
	popal
	ret
#}}}
#{{{err_msg
err_msg:
	cmpl $0xf,%eax
	jne 1f
	leal err01,%ecx
	movl $len01,%edx
	movl $1,%ebx
	jmp 9f
1:	
	cmpl $1,%eax
	jne	2f
	leal err02,%ecx
	movl $len02,%edx
	movl $1,%ebx
	jmp 9f
2:
	leal err03,%ecx
	movl $len03,%edx
	movl $1,%ebx
9:	
	movl $4,%eax
	int $0x80
	movl $0,%ebx
	movl $1,%eax
	int $0x80
	ret
#}}}	

#{{{chg_hex	 for %x %X
#used register:ecx,edi;protected:esi,ebx
chg_hex:
	call zero_buffer
	pushal
	movl $7,%ebx
#	movl s_ax(%esp),%eax		#get 'x or 'X
	cmpb $'X,%al
	je 1f
	addl $0x20,%ebx			#x
1:
	movl $0,%eax
	movl $8,%ecx
	leal buf,%edi
	cld
1:
	roll $4,%edx
	movb %dl,%al
	andb $0xf,%al
	cmpb $0,%al
	jne 2f
	decl %ecx
	jmp 1b
2:
	rorl $4,%edx
	movl %ecx,%esi			#save length
1:	
	roll $4,%edx
	movb %dl,%al
	andb $0xf,%al
	cmpb $0xa,%al
	jb 2f
	addb %bl,%al
2:
	addb $0x30,%al
	stosb
	loop 1b
	movl %esi,%ebx
	addl s_cx(%esp),%esi
	cmpl val,%esi
	jb 3f
	popal
	movl $0xf,%eax
	ret
3:	
	leal buf,%esi
	movl s_di(%esp),%edi
	movl %ebx,%ecx
	rep movsb
	movl %edi,s_di(%esp)		#update edi
	addl %ebx,s_cx(%esp)		#update length
	popal
	xorl %eax,%eax
	ret
#}}}	

#{{{chg_int		for %d
chg_int:
	call zero_buffer
	pushal
	movl $0,%esi
	testl $0x80000000,%edx
	jz	1f
	notl %edx
	incl %edx
	movl $1,%esi
1:
	leal buf,%edi
	addl $20,%edi
	movl $10,%ebx
	movl $0,%ecx
	movl %ecx,%eax
	xchgl %eax,%edx
	std
1:
	incl %ecx
	divl %ebx
	xchgl %eax,%edx
	addb $0x30,%al
	stosb
	movl $0,%eax
	xchgl %edx,%eax
	cmpl $0,%eax
	ja 1b
	cmpl $0,%esi
	je 2f
	incl %ecx
	movb $'-',%bl
	stosb
2:
	cld
	incl %edi
	movl s_di(%esp),%esi
	xchgl %esi,%edi
	movl %ecx,%ebx
	addl s_cx(%esp),%ecx
	cmpl val,%ecx
	jb 3f
	popal
	movl $0xf,%eax
	ret
3:	
	movl %ebx,%ecx
	rep movsb
	movl %edi,s_di(%esp)
	addl %ebx,s_cx(%esp)
	popal
	xorl %eax,%eax
	ret
#}}}	

#{{{chg_uint	for %u	test use pushal method
#can't be changed: esi,ebx
#modified:edi,ecx	+0,+24
chg_uint:
	call zero_buffer
	pushal
	movl $0,%eax
	leal buf,%edi
	addl $20,%edi
	xchgl %eax,%edx
	movl $10,%ebx
	movl $0,%ecx
	std
1:
	incl %ecx
	divl %ebx
	xchgl %eax,%edx
	addb $0x30,%al
	stosb
	xorl %eax,%eax
	xchgl %eax,%edx
	cmpl $0,%eax
	ja 1b
	cld
	incl %edi
	movl s_di(%esp),%esi
	xchgl %esi,%edi
	movl %ecx,%ebx			#save current legth for update string legth
	addl s_cx(%esp),%ecx
	cmpl val,%ecx
	jb 2f
	popal
	movl $0xf,%eax
	ret
2:
	movl %ebx,%ecx
	rep movsb
	addl %ebx,s_cx(%esp)		#update string length
	movl %edi,s_di(%esp)		#save new edi
	popal
	xorl %eax,%eax
	ret
#}}}	

#{{{chg_oct	for %o
chg_oct:
	call zero_buffer
	pushal
	movl $0,%eax
	leal buf,%edi
	addl $32,%edi
	xchgl %eax,%edx
	movl $8,%ebx
	movl $0,%ecx
	std
1:
	incl %ecx
	divl %ebx
	xchgl %eax,%edx
	addl $0x30,%eax
	stosb
	xorl %eax,%eax
	xchgl %eax,%edx
	cmpl $0,%eax
	ja 1b
	cld
	incl %edi
	movl s_di(%esp),%esi
	xchgl %esi,%edi
	movl %ecx,%ebx
	addl s_cx(%esp),%ecx
	cmpl val,%ecx
	jb 2f
	popal
	movl $0xf,%eax
	ret
2:	
	rep movsb
	addl %ebx,s_cx(%esp)
	movl %edi,s_di(%esp)
	popal
	xorl %eax,%eax
	ret
#}}}

#{{{chg_chr	for %c
chg_chr:
	call zero_buffer
	pushal
	incl %ecx
	cmpl val,%ecx
	jb 1f
	popal
	movl $0xf,%eax
	ret
1:
	movl %edx,%eax
	cmpb $32,%al
	jb 9f
	cmpb $126,%al
	ja 9f
	stosb
	movl %edi,s_di(%esp)
	movl %ecx,s_cx(%esp)
9:
	popal
	xorl %eax,%eax
	ret
#}}}	

#{{{chg_str for %s
chg_str:
	call zero_buffer
	pushal
	movl %edx,%esi
	movl $0,%ecx
	cld
1:
	lodsb
	incl %ecx
	cmpb $0,%al
	jne 1b
	decl %ecx
	movl %ecx,%eax
	addl s_cx(%esp),%eax
	cmpl val,%eax
	jb 2f
	popal
	movl $0xf,%eax
	ret
2:
	movl %ecx,%ebx
	movl %edx,%esi
	rep movsb
	movl %edi,s_di(%esp)
	addl %ebx,s_cx(%esp)
	popal
	xorl %eax,%eax
	ret
#}}}	

#{{{chg_ptr	for %p
chg_ptr:
	call zero_buffer
	pushal
	movl %edx,%esi
	movl $0,%ecx
	movl $0,%eax
	cld
#	repne scacb
1:
	lodsb
	incl %ecx
	cmpb $0,%al
	jne 1b
	decl %ecx
	movl %ecx,%eax
	addl s_cx(%esp),%eax
	cmpl val,%eax
	jb 2f
	popal
	movl $0xf,%eax
	ret
2:
	movl %ecx,%ebx
	movl %edx,%esi
	movl s_di(%esp),%edi
	rep movsb
	movl %edi,s_di(%esp)
	addl %ebx,s_cx(%esp)
	popal
	xorl %eax,%eax
	ret
#}}}	

#{{{_snprintf	return=0 success;
_snprintf:
	pushl %ebp
	movl %esp,%ebp
	xorl %ecx,%ecx		#current length of new string
	xorl %ebx,%ebx		#current index of %
	movl 8(%ebp),%edi
	movl 12(%ebp),%eax
	movl %eax,val
	movl 16(%ebp),%esi	#first,get const str;for fetch counts '%'
1:
	lodsb
	cmpb $'%',%al
	je 3f
	stosb
	incl %ecx
	cmpl val,%ecx
	jb 2f
	movl $1,%eax		#return 1
	jmp 9f
2:
	cmpb $0,%al
	jne 1b
	xorl %eax,%eax		#return 0
	jmp 9f
3:
	incl %ebx			#index of %
	lodsb
	movl 16(%ebp,%ebx,4),%edx		#transfer param by register
	cmpb $'d,%al		#%d
	jne 4f
#	pushl %esi						#protect used register
#	pushl %ebx						#protect
	call chg_int					#call subfunction
#	popl %ebx						#restore resgister
#	popl %esi						#restore
	cmpl $0,%eax					#check return
	je	1b
	call err_msg
	jmp 9f
4:	
	cmpb $'u,%al		#u
	jne 5f
	call chg_uint
	cmpl $0,%eax
	je 1b
	call err_msg
	jmp 9f
5:
	cmpb $'o,%al		#o
	jne 6f
	call chg_oct
	cmpl $0,%eax
	je 1b
	call err_msg
	jmp 9f
6:
	cmpb $'x,%al
	jne 7f
	call chg_hex
	cmpl $0,%eax
	je 1b
	call err_msg
	jmp 9f
7:
	cmpb $'X,%al
	jne 8f
	call chg_hex
	cmpl $0,%eax
	je 1b
	call err_msg
	jmp 9f
8:
	cmpb $'c,%al
	jne 2f
	call chg_chr
	cmpl $0,%eax
	je 1b
	call err_msg
	jmp 9f
2:
	cmpb $'s,%al
	jne 3f
	call chg_str
#	jmp 4f
	cmpl $0,%eax
	je 1b
	call err_msg
	jmp 9f
3:
	cmpb $'p,%al
	jne 4f
	call chg_ptr
	cmpl $0,%eax
	je 1b
	call err_msg
	jmp 9f
4:
	movl $1,%eax
	call err_msg
9:
#	movl $0,%ebx
#	movl $1,%eax
#	int $0x80
	movl %ebp,%esp
	popl %ebp
	ret
#}}}	

