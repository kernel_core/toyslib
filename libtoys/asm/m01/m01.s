.code16
BOOTSEG	=	0x7c0
DISPSEG	=	0xb800
SMAP	=	0x534d4150
D_MOD	=	3
.text
	jmp	$BOOTSEG,$go
go:
	mov %cs,%ax
	mov %ax,%ds
	mov %ax,%es
	lss stk,%sp
	cld
	call set_dispmod
	xor	%ax,%ax
	call clsscr
	xor %ax,%ax
	call show_msg
	xor %ax,%ax
	call get_phymem
	mov $0x200,%di
	mov $0x112,%ax
	mov %ax,(%di)
	mov $0x233,%ax
	mov %ax,2(%di)
	mov $0x200,%si
	lodsw
	mov %ax,%bx
	lodsw
	jmp .

#{{{ function set_dispmod
set_dispmod:
	push %ds
	mov $0x40,%ax
	mov %ax,%ds
	mov $0x49,%si
	lodsb
	cmp $D_MOD,%al
	je 1f
	mov $D_MOD,%ax
	int $0x10
1:
	pop %ds
	ret
#}}}
#{{{ function clsscr
clsscr:
	push %es
	mov $DISPSEG,%ax
	mov %ax,%es
	mov $0x720,%ax
	xor %di,%di
	mov $0x2000,%cx
	rep stosw
	pop %es
	ret
#}}}	
#{{{ function show_msg
show_msg:
	push %es
	mov $DISPSEG,%ax
	mov %ax,%es
	mov cursor,%di
	lea msg,%si
	mov $len,%cx
	mov $0xc,%ah
1:
	lodsb
	stosw
	loop 1b
	mov $len,%cx
	add %cx,cursor
	call set_cursor
	pop %es
	ret
#}}}
#{{{ function set_cursor:
set_cursor:
	mov cursor,%bx
	mov $0xf,%al
	mov $0x3d4,%dx
	outb %al,%dx
	inc %dx
	jmp .+2
	jmp .+2
	mov %bl,%al
	outb %al,%dx
	jmp .+2
	jmp .+2
	mov $0xe,%al
	mov $0x3d4,%dx
	outb %al,%dx
	jmp .+2
	jmp .+2
	inc %dx
	mov %bh,%al
	outb %al,%dx
	ret
#}}}
#{{{ function get_phymem
get_phymem:
	push %ds
	pop %es
	movl $0,%ebx
	movl $20,%ecx
	movl $SMAP,%edx
	movl %ebx,%esi
1:
	movl $0x200,%edi
	movl $0xe820,%eax
	int $0x15
	jc 2f
	movl 8(%edi),%eax
	addl %eax,%esi
	cmpl $0,%ebx
	jne 1b
	jmp 3f
2:
	movl $0,%esi
3:
	testl $0x8000,%esi
	jz 4f
	addl $0x8000,%esi
4:
	xorl %edi,%edi
	les stk,%edi
	incl %edi			#save at upper stack
	movl %esi,%eax
	stosl
	ret
#}}}	




cursor:	.word	160
stk:	.word	0x3ff,0x8000
msg:	.ascii	"hello world!"
len=.-msg
.org	510
.word	0xaa55

