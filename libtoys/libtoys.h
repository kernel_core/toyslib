#include"clsscr.h"

static char b64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

//struct 
/* int judge_utf8(char *str)	
功能：		用于判断给定的字符串中是否含有utf-8格式的字符
参数：		输入的待判断的字符串指针。输入的字符串长度<4096
返回值：	类型为整形
			>0：表示含有，具体值表示utf-8字符的数量
			=0：表示仅含有ASCII的字符串
			<0：表示不含有utf-8格式的字符串

   

 */
int judge_uft8(char *str);
char* base64_encode(char *p);
char* base64_decode(char *p);


